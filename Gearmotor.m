classdef Gearmotor
    % IN == 1
    % OUT == 2
    properties
        %% Gears
        G_1;
        G_2;
        
        %% Parameters
        a;                    % distance among axieses
        ap;                 % apparential distance among axieses
        aw;                 % real distance among axieses
        c_prim;            % stiffness coefficient
        epsilon;           % interlocking coefficient
        Gamma;
        Gamma_A;
        Gamma_B;
        Gamma_D;
        Gamma_E;
        i;                    % kinematic gear
        k;                    % earse slack coefficient
        u;                  % geometrical gear
        qm;
        rho;                % radius of teeth
        rho_Gamma;
        Tf;
        vs;                 % slip speed
        vs_abs;
        vs2v;
        x;                  % shift outline of the tooth coefficient
        X_alpha;
        XB;
        X_Gamma;
        XM;
        
        nu = 233*10^-3;  %https://physics.info/viscosity/
        mu;
        Rz100;
        ZM;
    end
    
    methods
        function obj = axiesesDistance(obj)
            %% method "axiesesDistance" is 
            %   used to count distance among 
            %   axieses
            
            % 1. Check
            if (isempty(obj.G_1.d) || isempty(obj.G_2.d))
                return
            end
            % 2. Calculations
            % 2.1 Base distance
            obj.a = (obj.G_1.d+obj.G_2.d)/2;
            % 2.2 Correction
            obj.x = obj.G_1.x + obj.G_2.x;
            % 2.3 Fundamental distance
            obj.ap = obj.a+obj.x*obj.G_1.m;
            % 2.4 Earse slack coeffiient
            obj.k = obj.x;
            % 2.5 Real distance
            obj.aw = obj.ap - obj.k*obj.G_1.m;
            
            obj.G_1 = obj.G_1.slackCorrection(obj.k);
            obj.G_2 = obj.G_2.slackCorrection(obj.k);
        end
        
        function obj = coefficientInterlocking(obj)
            %% method "coefficientInterlocking" is
            %   used to calcualte total interlocking among 
            %   the gears
            
            % 0. Execute
            obj.G_1 = obj.G_1.coefficientInterlocking;
            obj.G_2 = obj.G_2.coefficientInterlocking;
            % 1. Check
            if (isempty(obj.G_1.epsilon) ||...
                    isempty(obj.G_2.epsilon))
                return
            end
            % 2. Calculations
            % 2.1 Interlocking
            obj.epsilon = obj.G_1.epsilon+obj.G_2.epsilon;
            % 2.2 Stiffness coefficient
            % 2.2.1 Coefficients
            c = [.04723 .1551 .25791 -.00635...
                -.11654 -.00193 -.24188 .00529...
                .00182];
            % 2.2.2 q coefficient
            q = [1 1/obj.G_1.zv 1/obj.G_2.zv...
                obj.G_1.x obj.G_1.x/obj.G_1.zv...
                obj.G_2.x obj.G_2.x/obj.G_2.zv...
                obj.G_1.x^2 obj.G_2.x^2];
            % 2.2.3 Multiplicate
            q = q*c';
            % 2.2.4 Stiffness parameter
            obj.c_prim = 1/q;
            % 3. Interlocking stiffness
            obj.G_1 = obj.G_1.coefficientInterlockingStiffness(obj.c_prim);
            obj.G_2 = obj.G_2.coefficientInterlockingStiffness(obj.c_prim);
        end
        
        function obj = coefficientMaterial(obj)
            %% method "coefficientMaterial" is
            %   used to calcualte ZM
            
            % 1. Check
            if isempty(obj.G_1.E) || isempty(obj.G_2.E) ||...
                    isempty(obj.G_1.nu) || ...
                    isempty(obj.G_2.nu)
                return
            end
            % 2. Calculations
            m1 = (1-obj.G_1.nu^2)/obj.G_1.E*10^6;
            m2 = (1-obj.G_2.nu^2)/obj.G_2.E*10^6;
            obj.ZM = sqrt(1/pi/(m1+m2));
            
            obj.G_1.ZM = obj.ZM;
            obj.G_2.ZM = obj.ZM;
            % 2.2 Roughness
            obj.Rz100 = (obj.G_1.Rz+obj.G_2.Rz)*10^6/2*...
                (100/obj.a)*(1/3);
            
            obj.G_1.Rz100 = obj.Rz100;
            obj.G_2.Rz100 = obj.Rz100;
        end
        
        function obj = diameterRolling(obj)
            %% method "diameterRolling" is used
            %   to calculate rolling diameters of gears
            
            % 1. Check
            if (isempty(obj.aw) || isempty(obj.i))
                return
            end
            % 2. Calculations
            r_w1 = obj.aw*1/(1+obj.i);
            r_w2 = obj.aw*obj.i/(1+obj.i);
            
            obj.G_1.dw = 2*r_w1;
            obj.G_2.dw = 2*r_w2;
            
            obj.G_1.alpha_w = obj.G_1.angle(obj.G_1.dw);
            obj.G_2.alpha_w = obj.G_2.angle(obj.G_2.dw);
            
            obj.G_1.alpha_wt = acos(obj.a/obj.aw*cos(obj.G_1.alpha_t));
            obj.G_2.alpha_wt = acos(obj.a/obj.aw*cos(obj.G_2.alpha_t));
            
            obj.G_1.rho = obj.G_1.dw*sin(obj.G_1.alpha_wt)/2/cos(obj.G_1.beta_b);
            obj.G_2.rho = obj.G_2.dw*sin(obj.G_2.alpha_wt)/2/cos(obj.G_2.beta_b);
            
            obj.rho = (obj.G_1.rho*obj.G_2.rho)/(obj.G_1.rho+obj.G_2.rho);
            
            obj.Gamma_A = -obj.u*(obj.G_2.alpha_at/obj.G_1.alpha_wt-1);
            obj.Gamma_D = obj.Gamma_A+2*pi/obj.G_1.z/tan(obj.G_1.alpha_wt);
            obj.Gamma_E = tan(obj.G_1.alpha_at)/tan(obj.G_1.alpha_wt)-1;
            obj.Gamma_B = obj.Gamma_E - 2*pi/obj.G_1.z/tan(obj.G_1.alpha_wt);
            dG = (obj.Gamma_E-obj.Gamma_A)/1000;
            obj.Gamma = obj.Gamma_A:dG:obj.Gamma_E;
            
            obj.rho_Gamma = (1+obj.Gamma).*(obj.u-obj.Gamma)./(1+obj.u)^2*...
                obj.a*sin(obj.G_1.alpha_wt)/cos(obj.G_1.beta_b);
            
            obj.G_1 = obj.G_1.toothTemperature(obj.a,obj.rho,obj.u);
            obj.G_2 = obj.G_2.toothTemperature(obj.a,obj.rho,obj.u);
            obj.G_1.vt = obj.G_1.v*(1+obj.Gamma).*sin(obj.G_1.alpha_wt);
            obj.G_2.vt = obj.G_2.v*(1+obj.Gamma./obj.u)*sin(obj.G_2.alpha_wt);
            
            obj.vs_abs = abs(obj.G_1.vt-obj.G_2.vt);
            
        end
        
        function obj = forceGear(obj,N,Ka)
            %% method "forceGear" is used to
            %   calculate forces among the geras
            %   in fact to gear
            % N - power of suply, W
            % Ka - power suply coefficient
            
            % 1. Check
            if isempty(obj.G_1.da) || isempty(obj.G_2.da)
                return
            end
            % 2. Calcualtions
            obj.G_1 = obj.G_1.forcePower(N,Ka);
            obj.G_2 = obj.G_2.forceReceive(Ka,obj.i,obj.G_1.Pw);
            
            % 2.1 Weight Hertz flattening
            m1 = (1-obj.G_1.nu^2)/obj.G_1.E*10^6;
            m2 = (1-obj.G_2.nu^2)/obj.G_2.E*10^6;
            obj.G_1.bh = 4*sqrt(obj.G_1.Pn*obj.G_1.rho/...
                pi/obj.G_1.bL*(m1+m2));
            obj.G_2.bh = 4*sqrt(obj.G_2.Pn*obj.G_2.rho/...
                pi/obj.G_2.bL*(m1+m2));
            
            obj.G_1 = obj.G_1.coefficientKvMethodC;
            obj.G_2 = obj.G_2.coefficientKvMethodC;
            
            obj.G_1 = obj.G_1.toothForceRound;
            obj.G_2 = obj.G_2.toothForceRound;
            
            obj.G_1 = obj.G_1.coefficientKH_betaMethodD;
            obj.G_2 = obj.G_2.coefficientKH_betaMethodD;
            
            obj.G_1 = obj.G_1.coefficientKF_beta;
            obj.G_2 = obj.G_2.coefficientKF_beta;
            
            obj.G_1 = obj.G_1.coefficientK_BFH_alphaMethodC;
            obj.G_2 = obj.G_2.coefficientK_BFH_alphaMethodC;
            
            obj.G_1 = obj.G_1.toothForceReaction(obj.u);
            obj.G_2 = obj.G_2.toothForceReaction(obj.u);
            
            Ra = (obj.G_1.Ra+obj.G_2.Ra)/2;
            sum = (obj.G_1.vt+obj.G_2.vt).^(-1);
            obj.mu = ((obj.G_1.Ph_o*10^-6*sum./obj.nu*10^-3).^.25*.12).*(Ra*10^6*(obj.rho_Gamma.*10^3).^-1).^.25;
            
            obj.qm = obj.mu.*obj.vs_abs.*obj.G_1.wt/obj.G_1.bL/obj.G_1.bh;
            obj = obj.gammaTypeA;
            
            Xm1 = (2/(m1+m2))^.25;
            Xm2 = sqrt(1+obj.Gamma);
            Xm3 = sqrt(1-obj.Gamma./obj.u);
            XmL = Xm1.*Xm2.*Xm3;
            
            B1 = sqrt(obj.G_1.lambda*obj.G_1.cV);
            B2 = sqrt(obj.G_2.lambda*obj.G_2.cV);
            Xm4 = B1*sqrt(1+obj.Gamma);
            Xm5 = B2*sqrt(1-obj.Gamma./obj.u);
            XmM = Xm4+Xm5;
            
            obj.XM = XmL./XmM;
            
            XbL = .51*sqrt(1+obj.u)*abs(XmL);
            Xb1 = (1+obj.Gamma).^.25;
            Xb2 = (obj.u-obj.Gamma).^.25;
            XbM = Xb1.*Xb2;
            obj.XB = XbL./XbM;
            
            obj.X_alpha = 1.22*sin(obj.G_1.alpha_wt)^.25/cos(obj.G_1.alpha_wt)^.75*cos(obj.G_1.beta_b)^.25;
            
            obj.Tf = obj.mu.*obj.XM.*obj.XB.*obj.X_alpha;
            obj.Tf = obj.Tf.*obj.X_Gamma.*obj.G_1.wt^.75.*sqrt(obj.vs_abs)./sqrt(obj.a);
        end
        
        function obj = gearGeometrical(obj,u)
            %% method "gearGeomtrical" is
            %   used to calcualte quantity of teeth
            %   in second gear and rotation speed
            %   in this one. 
            % u - gear
            
            % 1. Check
            if isempty(obj.G_1.z) ||...
                isempty(obj.G_1.w) || ...
                   ~(isnumeric(u) && (u>0))
                return
            end
            % 2. Copy parameters
            obj.u =u;
            obj.i = obj.u;
            % 3. Calculations
            % 3.1 Minimal noumber of teeth
            z = obj.u*obj.G_1.z;
            z = ceil(z);
            za = obj.G_2.teethAlternative(z);
            if za < obj.G_2.zg
                obj.u = [];
                obj.i = [];
                return
            end
            obj.G_2.z = z;
            obj.G_2.zv = za;
            % 3.2 Diameters in sec. gear
            obj.G_2 = obj.G_2.diameters;
            % 3.3 Kinematic gear
            w = obj.G_1.w/obj.i;
            obj.G_2 = obj.G_2.speedRotation(w);
            % 3.4 Slack speed
            obj = obj.speedSlack;
        end
        
        function obj = gearKinematic(obj,w2)
            %% method "gearKinematic" is
            %   used to calcualte kinematic 
            %   gear among the gears
            % w2 - output rotation speed, rad/s
            
            % 1. Check
            if isempty(obj.G_1.z) ||...
                isempty(obj.G_1.w) || ...
                   ~(isnumeric(w2) && (w2>0))
                return
            end
            % 2. Calculations
            % 2.1 Gear
            obj.i  = obj.G_1.w/w2;
            
            obj = obj.gearGeometrical(obj.i);
        end
        
        function obj = Gearmotor
            %% constructor
            obj.G_1 = HelicalGears;
            obj.G_2 = HelicalGears;
        end
        
        function obj = geometry(obj,alpha,b,beta,c_,m,x1,x2)
            %% method "geometry" is used to calculate
            %   gears geometry
            % alpha - flank angle, deg
            % b - thickness of gear, m
            % beta - slope angle, deg
            % c_ - slack coefficient
            % m - modulus, m
            % x1 - swift of gear IN, m
            % x2 - swift of gear OUT, m
            
            % 1. Check
            if ~(isnumeric(alpha) && (alpha>0) &&...
                    isnumeric(b) && (b>0) && ...
                    isnumeric(beta) && isnumeric(c_) && ...
                    (c_>0) && isnumeric(m) && (m>0) && ...
                    isnumeric(x1) && isnumeric(x2))
                return
            end
            % 2. Geom. parameters
            obj.G_1 = obj.G_1.coefficientGeometrical(alpha,c_);
            obj.G_2 = obj.G_2.coefficientGeometrical(alpha,c_);
            
            obj.G_1 = obj.G_1.geometricalParameters(b,beta,m,x1);
            obj.G_2 = obj.G_2.geometricalParameters(b,beta,m,x2);
            end
    end
    
    
    
    methods(Access = private)
        function obj = speedSlack(obj)
            %% method "speedSlack" is used
            %   to calcualte slack speed among the
            %   gears from -m to m distance of 
            %   working point
            
            % 1. Check
            if isempty(obj.G_1.m) || ...
                    isempty(obj.G_1.w) || ...
                    isempty(obj.G_2.w)
                return
            end
            % 2. Calculation
            % 2.1 Step
            m = obj.G_1.m;
            dm = m/100;
            % 2.2 Distance vector
            M = -m:dm:m;
            % 2.3 Speed
            obj.vs = (obj.G_1.w+obj.G_2.w)*M;
            % 2.4 Speed to speed
            obj.vs2v = obj.vs./obj.G_1.v;
        end
        
        function obj = gammaTypeA(obj)
            
            % 2. Calcualtions
            GLen = length(obj.Gamma);
            obj.X_Gamma = zeros([1 GLen]);
            for iG = 1:GLen
                if obj.Gamma(iG) < obj.Gamma_B
                    obj.X_Gamma(iG) = 1/3*(1+(obj.Gamma(iG)-obj.Gamma_A)/...
                        (obj.Gamma_B-obj.Gamma_A));
                elseif obj.Gamma(iG) < obj.Gamma_D
                    obj.X_Gamma(iG) = 1;
                elseif obj.Gamma(iG) < obj.Gamma_E
                    obj.X_Gamma(iG) = 1/3*(1+(obj.Gamma_E-obj.Gamma(iG))/...
                        (obj.Gamma_E-obj.Gamma_D));
                end
            end
            
        end
    end
end