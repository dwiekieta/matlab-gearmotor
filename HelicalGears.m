classdef HelicalGears
    properties(Constant)
        %% 1. Nominal modulus
        % 1. First sequence (PN-78/M-88502)
        m_I = [.05,.06,.08,.1,.12,.15,.2,.25,.3,.4,.5,.6,.8,...
            1,1.25,1.5,2,2.5,3,4,5,6,8,10,12,16,20,25,...
            32,40,50,80,100]*10^-3;
        % 2. Second sequence
        m_II = [.055,.07,.09,.11,.14,.18,.22,.28,.35,...
            .45,.55,.7,.9,1.125,1.375,1.75,2.25,2.75,...
            3.5,4.5,5.5,7,9,11,14,18,22,28,36,45,55,...
            60,70,90]*10^-3;
        
        %% 2. Const. geometrical coefficients
        ha_ = 1;        % coefficient of the head tooth height
        hl_ = 2;        % coefficient of the limit height tooth
        s_ = pi/2;      % coefficient of the thickness
    end
    
    properties    
        %% Mechanical parameters
        cV = 3.8;                               % [N/mm^2/K]
        E = 21*10^10;                       % [Pa], Young's module'
        lambda = 41;                        % [N/s/K]
        nu = 0.3;                                  % [-], Poisson's coefficient
        Ra = 3*10^-6;
        Re = 275*10^6;
        Rz = 3*10^-6;                     % [m], Roughness
        Rz100;
        sigma_HLim = 35*10^7;        % Limit of stress
        sigma_FLim = 25*10^7;      % notch strength
        
        %% Geometrical coefficients
        c_;                                             % coefficient of the slack 
        h_ ;                                            % Coefficient of the tooth height
        hf_ ;                                           % Coefficient of the food tooth height
        hw_;                                          % Coefficient of the depth of penetretion
        rho_f_;                                          % Coefficient of the curve radius
        
        %% Geometrical parameters
        alpha;                                        % flank angle
        alpha_t;                                    % leading outline angle
        alpha_at;
        alpha_w;                                    % rolling angle
        alpha_wn;
        alpha_wt;                                      % leading rolling angle
        b;                                                  % gear thickness 
        bh;                                                 % width hertz flattening
        bL;                                                 % gear tooth length
        beta;                                           % slope angle
        beta_b;
        beta_w;                                         % rolling slope angle
        c;                                                % slack 
        c_gamma;                                % interlocking stiffness
        c_prim;                                     % correct slack
        d;                                              % divided circle
        da;                                             % heads diameter
        db;                                             % fundamental diameter
        df;                                              % feet diameter
        dw;                                             % rolling diameter
        e;                                                  % tooth geometrical parameter
        epsilon;                                    % essential interlocking coefficient
        epsilon_alpha;                          % ess. leading in.-lo. coeff.
        epsilon_beta;                           % ess. step in.-lo. coeff.
        Gamma;                                  % coordinate
        h;                                            % tooth height
        ha;                                           % head tooth height
        hf ;                                           %food tooth height
        hl;                                             % limit of  tooth height
        hw;                                          % depth of penetretion
        inv_alpha;                              % involute
        m;                                              % module
        mt;                                             % leading module 
        p;                                                % tooth pitch
        pt;                                                 % leading tooth pitch
        p_bL;                                               
        s;                                                  % tooth thickness
        sa_min;                                         % min. thockness of tooth
        sk;                                                     % division thickness
        rho;                                                % tooth radius
        rho_a0;                                         % curve coefficient
        rho_f;                                          % curve radius
        v=0;                                                  % linear speed
        vt;
        w=0;                                                  % rotation speed
        x;                                                  % outline shift coefficient
        z;                                                 % noumber of teeth
        zg;                                                 % limit noumber of teeth
        zv;                                                 % alternative noumber of teeth
        
        
        %% Main forces
        Pn;                                         % normaln complex force
        Pp;                                         % complex force
        Pr;                                         % radius force
        Ps;                                         % tangent force
        Pw;                                         % round force
        Px;                                         % axial force
        T;                                          % torque
        Te;                                          % reduced torque
        wt;
        
        %% Coefficients
        Ka;                                     % overload coeff.
        Kv;                                     % dynamic forces coeff.
        KB_alpha;
        KF_alpha;
        KH_alpha;  
        KB_beta;
        KF_beta;
        KH_beta;  
        KB_gamma;
        SH_min = 1.3;                           % safety coefficient
        Y_beta;                                         % notch coeff
        Y_dRelT;
        Y_epsilon;                                  % set point coefficient
        Y_Fa;                                           % shape coefficient
        Y_NT = 1;
        Y_rRelT = 1.12;
        YS;
        Y_Sa;                                           % notch coeff
        Y_ST = 2;
        YX;                                             % widness coeff
        Z_beta;                                         % slope coefficient
        Z_epsilon;                                    % interlocking coefficient
        ZM;
        ZH;                                             % contact coefficient
        Z_NT = 1;                                       % durability coefficient
        ZL = 1;
        ZR = 1;
        Zv = 1;
        ZW = 1.1;
        ZX;
        
        % Forces
        Pb;                                     % seizure force
        Pb_o;
        Pf;                                      % bending force
        Pf_o;
        Ph;                                     % Tension force
        Ph_o;
        Pv;                                     % round force among the teeth
        sigma_c;                            % compression stress
        sigma_F;                               % base stress
        sigma_F0;
        sigma_g;                            % bending stress
        sigma_H;
        sigma_H0;
        sigma_z;                            % complex stress
        SF;
        SH;                                     % safety coeff.
        tau_m;                              % shear stress
    end
    
    methods
        function out = angle(obj,d)
            %% method "angle" is used to
            %   calcualte angle of the any diameter 
            % d - diameter, m
            
            % Check
            if ~isnumeric(d) ||...
                    isempty(obj.db)
                return
            end
            % 2. Calculations
            if d > obj.db
                out = acos(obj.db/d);
            else
                out = -acos(d/obj.db);
            end
        end
        
        function obj = coefficientGeometrical(obj,a,c)
            %% method "coefficientGeometrical" 
            %   is used to calculate geometrical 
            %   coefficients of helical gears
            % a - flank angle, deg
            %       typical a = 20
            % c - coefficient from PN-92/M-88503
            %       typical c = 0.25
            %       0.1 <= c <= 0.4
            
            % 1. Input correction check
            if ~(isnumeric(a) && (a>0) && ...
                    isnumeric(c) && (c>0))
                %out = -1;
                return
            end
            % 2. Copy parameters
            obj.alpha = deg2rad(a);
            obj.c_ = c;
            % 3. Coefficient of the food tooth height
            obj.hf_ = obj.ha_+obj.c_;
            % 4. Coefficient of the tooth height
            obj.h_ = obj.ha_+obj.hf_;
            % 5. Coefficient of the depth of penetretion
            obj.hw_ = obj.ha_+obj.hf_-obj.c_;
            % 6. Coefficient of the curve radius
            if obj.c_ <= .295
                obj.rho_f_ = obj.c_/(1-sin(obj.alpha));
            else
                obj.rho_f_ = (1+sin(obj.alpha))/cos(obj.alpha)*...
                    (pi/4-(1+obj.c_)*tan(obj.alpha));
            end
            % 7. Out
            %out = obj;
        end
        
        function obj = coefficientKF_beta(obj)
            %% method "coefficientKF_beta" is
            %   used to calculate KF_beta coefficient
            
            % 1. Check 
            if isempty(obj.KH_beta)
                return
            end
            % 2. Calculations
            % 2.1 Index
            b2h = obj.b/obj.h;
            vi = b2h^2/(1+b2h+b2h^2);
            % Coefficient
            obj.KF_beta = obj.KH_beta^vi;
        end
        
        function obj = coefficientK_BFH_alphaMethodC(obj)
            %% method "coefficientKbfh_alphaMethodC"
            %   is used to calculate KB_ KF_ KH_alpha
            
            % 1. Check
            if isempty(obj.Z_epsilon)
                return
            end
            % 2. Calculations
            if obj.beta == 0
                obj.KH_alpha = 1/obj.Z_epsilon^2;
                obj.KB_alpha = obj.KH_alpha;
            else
                obj.KH_alpha = obj.epsilon;
                obj.KB_alpha = obj.KH_alpha;
            end
            
            obj.KF_alpha = obj.epsilon/obj.epsilon_alpha/...
                obj.Y_epsilon;
            
            if obj.epsilon <= 2
                obj.KB_gamma = 1;
            elseif obj.epsilon < 3.5
                obj.KB_gamma = 1+.2*sqrt((obj.epsilon-2)*...
                    (5-obj.epsilon));
            else
                obj.KB_gamma = 1.3;
            end
        end
        
        function obj = coefficientKH_betaMethodD(obj)
            %% method "coefficientKH_betaMethodD"
            %   is used to calculate KH_beta coefficient
            %   of the grat in the 6. class of precision
            
            % 1. Check
            if isempty(obj.b) || isempty(obj.d) ||...
                    isempty(obj.c_gamma) || isempty(obj.h)
                return
            end
            
            if obj.c_gamma <= 15 || obj.c_gamma >= 25 
                nameOfMethod = 'coefficientKH_betaMethodD';
                nameOfCondition = 'c_gamma';
                value = num2str(obj.c_gamma);
                fprintf('Out of range: %s/%s = %s\n',nameOfMethod,nameOfCondition,value);
            end
            if obj.b <= .05 || obj.b >= .4 
                nameOfMethod = 'coefficientKH_betaMethodD';
                nameOfCondition = 'b';
                value = num2str(obj.b);
                fprintf('Out of range: %s/%s = %s\n',nameOfMethod,nameOfCondition,value);
            end
            if obj.b/obj.d >= 2  
                nameOfMethod = 'coefficientKH_betaMethodD';
                nameOfCondition = 'b/d';
                value = num2str(obj.b/obj.d);
                fprintf('Out of range: %s/%s = %s\n',nameOfMethod,nameOfCondition,value);
            end
            if obj.Pv/obj.d < 100000 ||   obj.Pv/obj.d > 400000
                nameOfMethod = 'coefficientKH_betaMethodD';
                nameOfCondition = 'Pv/b';
                value = num2str(obj.Pv/obj.d);
                fprintf('Out of range: %s/%s = %s\n',nameOfMethod,nameOfCondition,value);
            end
            if obj.b/obj.h <= 3 || obj.b/obj.h >= 12 
                nameOfMethod = 'coefficientKH_betaMethodD';
                nameOfCondition = 'b/h';
                value = num2str(obj.b/obj.h);
                fprintf('Out of range: %s/%s = %s\n',nameOfMethod,nameOfCondition,value);
            end
            % 2. Calculations
            obj.KH_beta = 1.17+.18*(obj.b/obj.d)^2+.47*obj.b;
            
            obj.KB_beta = obj.KH_beta;
        end
        
        function obj = coefficientKvMethodC(obj)
            %% method "coefficientKvMethodC"
            %   is used to calculate Kv parameter
            %   from PN-79/M-88522/01
            
            % 1. Check
            if isempty(obj.v)
                return
            end
            % 2. Calculations
            vz = obj.v*obj.z/100;
            if obj.beta == 0
                obj.Kv = vz*1.6/10+1.4-6.5*1.6/10;
            else
                Kva = vz*1.4/12+0.5;
                Kvb = vz*1.6/10+1.4-6.5*1.6/10;
                if obj.epsilon_beta > 1
                    obj.Kv = Kva;
                else
                    obj.Kv = Kva - obj.epsilon_beta*(Kva-Kvb);
                end
            end
            % 3. Correction
            if obj.Kv > 1.5
                obj.Kv = 1.5;
            end
        end
        
        function obj = coefficientInterlocking(obj)
            %% method "coefficientInterlocking"
            %   is used to calculate essential interlocking
            %   coeeficinet of the gear
            
            % 1. Check
            if isempty(obj.alpha_wt)
                return
            end
            % 2. Calculations
            % 2.1 Partial interlocking coefficient
            obj.epsilon_alpha = obj.da/2/obj.p_bL*(...
                sqrt((obj.da/obj.dw)^2-cos(obj.alpha_wt)^2)-...
                sin(obj.alpha_wt));
            
            obj.epsilon_beta = obj.b*sin(obj.beta)/pi/obj.m;
            
            obj.epsilon = obj.epsilon_alpha+obj.epsilon_beta;
            
            obj.e = obj.epsilon_beta*obj.p_bL;
            % 2.2 Hardity coefficients
            if obj.beta == 0
                obj.Z_epsilon = sqrt((4-obj.epsilon_alpha)/3);
            else
                if obj.epsilon_beta < 1
                    obj.Z_epsilon = sqrt((4-obj.epsilon_alpha)*...
                        (1-obj.epsilon_beta)/3+obj.epsilon_beta/...
                        obj.epsilon_alpha);
                else
                    obj.Z_epsilon = sqrt(1/obj.epsilon_alpha);
                end
            end
            % 2.3 Set point coefficient
            obj.Y_epsilon = .25+.75/obj.epsilon_alpha*cos(obj.beta_b)^2;
            % 2.4 Longitudal coefficient
            obj.ZH = sqrt(2*cos(obj.beta_b)*cos(obj.alpha_wt)/...
                cos(obj.alpha_t)^2/sin(obj.alpha_wt));
            
            % 2.5 Notch coefficients
            La = obj.s(1)/obj.hf;
            qs = obj.s(1)/2/obj.rho_f;
            obj.Y_Sa = (1.2+.13*La)*qs^...
                (1/(1.21+2.3/La));
            if obj.epsilon_beta > 1
                eb = 1;
            else
                eb = obj.epsilon_beta;
            end
            if rad2deg(obj.beta) > 30
                ab = 30;
            else
                ab = rad2deg(obj.beta);
            end            
            obj.Y_beta = 1-eb*ab/120;
            obj.YS = obj.Y_Sa*(.6+.4*obj.epsilon_alpha);
            obj.Y_dRelT = (1+.93*(obj.YS-1)*(200/obj.Re*10^-6)^(1/4))/...
                (1+.93*(200/obj.Re*10^-6)^(1/4));
        end
        
        function obj = coefficientInterlockingStiffness(obj,c_prim)
            %% method "coefficientInterlockingStiffness" is
            % used to calcualte interlocking stiffness parameter
            % c_prim - stiffness coefficient
            
            % 1. Check 
            if ~isnumeric(c_prim) || isempty(obj.epsilon_alpha)
                return
            end
            % 2. Calculations
            obj.c_gamma = c_prim*(.75*obj.epsilon_alpha+.25);
        end
        
        function obj = diameters(obj)
            %% method "diameters" is used to
            %   calculate diameters of heads and 
            %   feet
            % d - diameter of division circle, m
            
            % 1. Input correction check
            if (isempty(obj.m) || ...
                   isempty(obj.z))
                return
            end
            % 2. Calculations
            obj.d = obj.mt*obj.z;
            obj.da = obj.d + 2*obj.ha;
            obj.db = obj.d*cos(obj.alpha);
            obj.df = obj.d - 2*obj.hf;
            % 2.3 Thickness
            obj = obj.thickness;
        end
        
        function obj = forcePower(obj,N,Ka)
            %% method "forcePower" is used to
            %   compute main forces in the gear
            % N - power of suply, W
            % Ka - overload coefficient
            
            % 1. Check
            if isempty(obj.w) || isempty(obj.dw)
                return
            end
            
            obj.Ka = Ka;
            % 2. Calculations
            % 2.1 Round force
            obj.Pw = 2*N/obj.w/obj.dw;
            % 2.2 Axial force
            obj.Px = obj.Pw*tan(obj.beta_w);
            % 2.3 Radius force
            obj.Pr = obj.Pw*tan(obj.alpha_wt);
            % 2.4 Complex force
            obj.Ps = obj.Pw/sin(obj.beta_w);            
            % 2.5 Complex force
            obj.Pp = obj.Pw/cos(obj.alpha_wt);
            % Angle
            obj.alpha_wn = atan(obj.Pr/obj.Ps);
            % 2.6 Complex force
            obj.Pn = obj.Pw/cos(obj.beta_w)/cos(obj.alpha_wt);
            % 2.7 Torque
            obj.T = N/obj.w;
            % 2.8 Equivalent torque
            obj.Te = obj.T*Ka;
            
            % 3. Shape coefficient
            obj.Y_Fa = 6*obj.hf*cos(obj.alpha_wn)/obj.m/...
                (obj.s(1)/obj.m)^2/cos(obj.alpha);
        end
        
        function obj = forceReceive(obj,Ka,i,pw)
            %% method "forceReceive" is used
            %   to calculate forces in the gerar in motion
            % Ka - overload coefficient
            % i - gear
            % pw - round force, N
            
            % 1. Check
            if isempty(obj.w) || isempty(obj.dw)
                return
            end
            % 2. Copy 
            obj.Ka = Ka;
            obj.Pw = pw;
            % 3. Calculations
            % 3.1 Axial force
            obj.Px = obj.Pw*tan(obj.beta_w);
            % 3.2 Radius force
            obj.Pr = obj.Pw*tan(obj.alpha_wt);
            % 3.3 Complex force
            obj.Ps = obj.Pw/sin(obj.beta_w);
            % 3.4 Complex force
            obj.Pp = obj.Pw/cos(obj.alpha_wt);
            % Angle
            obj.alpha_wn = atan(obj.Pr/obj.Ps);
            % 3.5 Complex force            
            obj.Pn = obj.Pw/cos(obj.beta_w)/cos(obj.alpha_wn);
            % 3.6 Torque
            obj.T = obj.Pw*obj.dw/2;
            % 3.7 Equivalent torque
            obj.Te = obj.T*obj.Ka/i;
            
            
            % 4. Shape coefficient
            obj.Y_Fa = 6*obj.hf*cos(obj.alpha_wn)/obj.m/...
                (obj.s(1)/obj.m)^2/cos(obj.alpha);
        end
        
        function obj = geometricalParameters(obj,b,beta,m,x)
            %% method "geometricalParameters" is used
            %   to calculate basic parameters of helical gear
            % b - gear thickness, m
            % beta - slope angle, deg
            %       8 <= b <= 17
            % m - modulus, m
            % x - shift outline coefficient, m
            
            % 1. Input correction check
            if ~(isnumeric(b) && (b>0) &&...
                    isnumeric(beta) && ...
                    isnumeric(m) && (m>0) && ...
                    isnumeric(x)) || ...
                isempty(obj.hf_)
                %out = -1;
                return
            end
            % 2. Copy parameter
            obj.b = b;
            obj.beta = deg2rad(beta);
            obj.beta_w = obj.beta;
            obj.beta_b = obj.beta;
            obj.m = m;
            obj.x = x;
            % 3. Leading modulus
            obj.mt = obj.m/cos(obj.beta);
            % 4. Leading outline angle
            obj.alpha_t = atan(tan(obj.alpha)/cos(obj.beta));
            obj.alpha_at = 1.001*obj.alpha_t;
            % 4. Calculating divided cicle
            obj.p = obj.m*pi;
            obj.pt = obj.mt*pi;
            obj.p_bL = obj.pt*cos(obj.alpha_t);
            % 5. Head tooth height
            obj.ha = obj.ha_*obj.m;
            % 6. Foot tooth height
            obj.hf = obj.hf_*obj.m;
            % 7. Tooth height
            obj.h = obj.h_*obj.m;
            % 8. Limit of the tooth height
            obj.hl = obj.hl_*obj.m;
            % 9. Tooth thickness
            obj.s = obj.s_*obj.m;
            obj.sa_min = .4*obj.m;
            obj.sk = (obj.s_+2*obj.x*tan(obj.alpha))*...
                obj.m;
            % 10. Slack between teeth
            obj.c = obj.c_*obj.m;
            % 11. Curve radius 
            obj.rho_f = obj.rho_f_*obj.m;
            % 12. Deepth of penetration
            obj.hw = obj.hw_*obj.m;
            % 13. Involute
            obj.inv_alpha = obj.involute(obj.alpha);
            % 14. Limit of teeth
            obj.zg = 2*(obj.ha_-obj.x)*cos(obj.beta)^3/sin(obj.alpha)^2;
            obj.zg = ceil(obj.zg);
            % 15. Length of tooth
            obj.bL = obj.b/cos(obj.beta_b);
            % 16. Slope coefficient
            obj.Z_beta = sqrt(cos(obj.beta));
            % 17. Load coefficient
            obj.ZX = 1.05-.005*obj.m;
            % 18. Curve coefficient
            obj.rho_a0 = .25*obj.m;
            % 19. Widness coeff
            if obj.m <= .005
                obj.YX = 1;
            elseif obj.m <= .03
                obj.YX = 1.03-6*obj.m;
            else
                obj.YX = .85;
            end
            % 18. Diameters
            obj = obj.diameters();
        end       
        
        function obj = slackCorrection(obj,k) 
            %% method "slackCorrection" is used
            %   to correction slack after tooth modify
            % k - earse slack coefficient
            
            % 1. Check 
            if isempty(obj.m) || ~isnumeric(k)
                return
            end
            % 2. Calculations
            obj.c_prim = (obj.c_-k)*obj.m;
            
            
            obj.ha = (obj.ha_+obj.x-k)*obj.m;
            obj.hf =  (obj.ha_-obj.x+obj.c_)*obj.m;
           
            obj = obj.diameters;
        end
        
        function obj = speedLinear(obj,v)
            %% method "speedLinear" is used 
            %   to calculate rotation speed of gear
            % v - linear speed, m/s
            
            % 1. Input correction check
            if ~(isnumeric(v) && (v~=0) && ...
                ~isempty(obj.d))
                return
            end
            % 2. Copy of parameters
            obj.v = v;
            % 2. Calculations
            obj.w = obj.v/obj.d*2;
        end
        
        function obj = speedRotation(obj,w)
            %% method "speedRotation" is used 
            %   to calculate linear speed of gear
            % w - rotation speed, rad/s
            
            % 1. Input correction check
            if ~(isnumeric(w) && (w~=0) && ...
                ~isempty(obj.d))
                return
            end
            % 2. Copy of parameters
            obj.w = w;
            % 2. Calculations
            obj.v = obj.d/2*obj.w;
        end
        
        function out = teethAlternative(obj,z)
            %% method "teethAlternative" is used to
            %   calculate alternative noumber of teeth in the gear
            % z - real noumber of teeth
            
            % 1. Check
            if isempty(obj.beta)
                return
            end
            % 2. Calculation
            out = z/cos(obj.beta)^3;
        end
        
        function obj = teethDiameter(obj,d)
            %% method "teethDiameter" is used
            %   to calcualte minimal quantity of teeth
            %   in the gear 
            % d - diameter, m
            
            % 1. Input correction check
            if ~(isnumeric(d) && (d>0) && ...
                ~isempty(obj.m))
                return
            end
            % 2. Real noumber of teeth
            obj.z = d/obj.m;
            obj.z = ceil(obj.z);
            % 3. Chceck diameter
            while true
                obj = obj.diameters();
                if obj.df < d
                    obj.z = obj.z + 1;
                else
                    break
                end                    
            end
            obj.zv = obj.teethAlternative(obj.z);
        end
        
        function obj = teethNoumber(obj,z)
            %% method "teethNoumber" is used
            %   to set teeth parameter and
            %   calculate parameters of gear 
            %   connected with.
            % z - noumber of teeth
            
            % 1. Input correction check
            if ~(isnumeric(z) && (z>0))
                return
            end
            % 2. Copy parameter
            obj.z = z;
            obj.zv = obj.teethAlternative(obj.z);
            % 3. Diameters
            obj = obj.diameters();
        end
        
        function obj = toothForceReaction(obj,u)
            %% method "toothForceReaction" is
            %   used to calculate reaction forces in 
            %   the tooth of the gear
            
            % 1. Check
            if isempty(obj.KB_beta) || isempty(obj.KH_alpha)
                return
            end
            % 2. Calculations
            % 2.1 Tension
            obj.Ph = obj.Pv*obj.KH_beta;
            obj.Ph_o = obj.Ph*obj.KH_alpha;
            % 2.2 Bending
            obj.Pf = obj.Pv*obj.KF_beta;
            obj.Pf_o = obj.Pf*obj.KF_alpha;
            % 2.3 Seizure condition
            obj.Pb = obj.Pv*obj.KB_beta;
            obj.Pb_o = obj.Pb*obj.KB_alpha;
            
            obj.wt = obj.Pb_o*obj.KB_gamma/obj.b;
            
            % 2.4 Hertz stress
            obj.sigma_H0 = obj.ZH*obj.ZM*obj.Z_epsilon*obj.Z_beta*...
                sqrt(obj.Pw/obj.d/obj.b*(u+1)/u);
            % 2.5 Tension stress
            obj.sigma_H = obj.sigma_H0*sqrt(obj.Ka*...
                obj.Kv*obj.KH_alpha*obj.KH_beta);
            % 2.6 Safety coefficient
            obj.SH = obj.sigma_HLim/obj.sigma_H*...
                obj.Z_NT*obj.ZL*obj.ZR*obj.Zv*obj.ZW*obj.ZX;
            
            
            % 2.7 Bending stress
            pc_bs = obj.Pn*cos(obj.beta_b)/obj.b/obj.s(1);
            obj.sigma_g = 6*pc_bs*cos(obj.alpha_wn)/obj.s(1);
            obj.sigma_c = pc_bs*sin(obj.alpha_wn);
            obj.sigma_z = obj.Pw/obj.b/obj.m*obj.Y_Fa*obj.Y_epsilon;
            obj.tau_m = pc_bs*cos(obj.alpha_wn);
            
            % 2.8 Base stress
            obj.sigma_F0 = obj.Pw/obj.b/obj.m*obj.Y_Fa*...
                obj.Y_Sa*obj.Y_epsilon*obj.Y_epsilon;
            obj.sigma_F = obj.sigma_F0*obj.Ka*obj.Kv*obj.KF_beta*obj.KF_alpha;
            % 2.9 Safety coefficinet
            obj.SF = obj.sigma_FLim/obj.sigma_F*...
                obj.Y_ST*obj.Y_NT*obj.Y_dRelT*obj.Y_rRelT*obj.YX;
        end
        
        function obj = toothForceRound(obj)
            %% method "toothForceRound" is used to
            %   calculate the round force int the gear
            
            % 1. Check
            if isempty(obj.Ka) || isempty(obj.Kv)
                return
            end
            % 2.Calculations
            % 2.1 Round force among the teeth
            obj.Pv = obj.Pw*obj.Ka*obj.Kv;
        end
        
        function obj = toothTemperature(obj,a,rho_,u)
            %% method "toothTemperature" is 
            %   used to calcualte temperature in tooth
            % a - distance among the teeth
            % rho_ - radius of Hertz cylinder
            % u - geometrical gear
            
            % 1, Check
            if isempty(obj.rho)
                return
            end
            % 2. Calculation
            % 2.1 Gamma coordinate
            R = rho_*(1+u)^2*cos(obj.beta_b)/...
                a/sin(obj.alpha_wt);
            bb = (u-1);
            delta = bb^2+4*u*R;
            Gamma1 = (-bb-sqrt(delta))/(-2);
            Gamma2 = (-bb+sqrt(delta))/(-2);
            if Gamma1>Gamma2
                obj.Gamma = Gamma1;
            else
                obj.Gamma = Gamma2;
            end
            
            
        end
    end
    
    methods(Access = private)
        function out = involute(~,a)
            %% method "involute" is used to
            %   calculate involute of angle
            % a - angle, rad
            
            % 1. Check
            if ~isnumeric(a)
                return
            end
            % 2. Calculations
            out = tan(a)-a;
        end
        
        function obj = thickness(obj)
            %% method "thickness" is used
            %   to calculate thickness of tooth 
            %   among fundamental diameter and 
            %   head diameter
            
            % 1. Check 
            if isempty(obj.db) || ...
                    isempty(obj.da)
                return
            end
            % 2. Calculations
            % 2.1 Step
            dd = (obj.da-obj.df)/200;
            % 2.2 Distance vector
            D = obj.df:dd:obj.da;
            DLen = length(D);
            obj.s = zeros([1 DLen]);
            % 2.3 Loop
            for ind = 1:DLen
                a_ar = obj.angle(D(ind));
                inv_ar = obj.involute(a_ar);
                obj.s(ind) = D(ind)*(obj.sk/obj.d+...
                    obj.inv_alpha-inv_ar);
            end
            obj.s(2,:) = D;
        end
    end
end