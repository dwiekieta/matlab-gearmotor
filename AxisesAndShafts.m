classdef AxisesAndShafts
    % Class AAS contain methods of computing
    properties(Constant)
        %% Nominal diameters of Axises and Shafts
        % 1. First sequence (PN-78/M-02041)
        dShafts_I = [6:2:18,22,24,25,30,32,35,...
            40,42,45:5:100,120,150,180,200,220,...
            250,280,300,320,350,380,400,420,...
            450,480,500,600]*10^-3;
        % 2. Second sequence
        dShafts_II = [28,38,48,110,130,140,...
            160,170,190,210,230,240,260,...
            270,290,310,330,340,360,370,390]*10^-3;
        %% Nominal diameters of Journal (czop)
        % 1. First sequence (PN-89/M-85000)
        dJournal_I = [6:12,14:2:18,20,22,25,28,...
            30,32,35,36,40,45,50,55,60,63,70,71,...
            80,90,100,110,125,140,160,180,200]*10^-3;
        % 2. Second sequence 
        dJournal_II = [19,24,38,48,53,58,75,...
            85,95,105,120,130,150,170,190]*10^-3;
    end
    
    properties
        a_d = 0;                              % Angle of defletion
        a_t = 0;                                % Angle of twist
        
        b = 0;                                 % Total bending
        
        d_b = 0;                            % Diameter of shaft from benfing
        d_I = 0;                               % Geometrical inertial moment
        d_s = 0;                              % Diameter of shaft
        d_st = 0;                            % Silhouette of shaft
        d_Wg = 0;                            % Strength indicator
        d_W0 = 0;                            % Strength indicator
        
        l_d = 0;                                % Line of deflection
        
        sigma = 0;                          % Stress from bending
        sigma_HM = 0;                   % Stress from Huber Mises theory
        sf = 0;                                     % Safety coefficient
        
        t = 0;                                  % Torque
        tau = 0;                               % Stress form torque
        
        x_rs = 0;                           % Reaction in supports
        x_ffx = 0;                          % Array of forces
        x_b = 0;                            % Bending 
        x_c = 0;                            % Cutting
        
        y_rs = 0;
        y_ffx = 0;
        y_b = 0;
        y_c = 0;
        
        w_kr;
    end
    
    methods
        function out = auto(obj,a,dJ,dL,dS,E,G,L,maxDA,maxDL,maxTA,n,xMX,xFAX,xqXX,yMX,yFAX,yqXX,Re,TX,u,xF,xS)
            %% method "auto" is used to
            %   compute axies or shaft
            %
            % a - Huber-Mises coefficient
            % dJ - diameters of journals, m
            % dL - step of length, m
            % dS - diameters of standard, m
            % E - Young's modulus, Pa
            % G - modulus of the lateral elasticy, Pa
            % L - length, m
            % maxDA - limit of the defraction angle from bending, rad
            % maxDL - limit of difraction line, m
            % max TA - limit of the twist angle from torque, rad
            % n - safety factor
            % x___ - in the x axies
            % y___ - in the y axies(gravity ones)
            % MX - array of bending moments
            %       [[m1,x1];...]
            %       m - bending moment, Nm
            %       x - set point, ,m
            % FAX - array of focused forces 
            %       [[f1,a1,x1];...]
            %       f - force, N
            %       a - angle, rad
            %       x - set point, m
            % qXX - array of continoused forces
            %       [[q1,x11,x12];...]
            %       q - elementary force. N/m
            %       xn1 - start point, m
            %       xn2 - stop point, m
            % Re - yeld strength, Pa
            % TX - array of torques
            %       [[t1,x1];...]
            %       t - torque, Nm
            %       x - set point, m
            % u - factor of the area force impact
            % xF - set point of floating support, m
            % xS - set point of static support, m
            %   Return is array
            %       [l1,l2,....;
            %           ]
            
            % 1. Reactions in the x axies
            % 1.1 Check if forces in x axies exiest
            if ~(isempty(xMX) && isempty(xFAX) && isempty(xqXX))
                % 1.1.1 Reaction in supports
                obj.x_rs = obj.reactionSupports(xS,xF,xFAX,xqXX,xMX(:,1));
                % 1.1.2 Array of forces
                obj.x_ffx = obj.reactionArray(xS,xF,xFAX,xqXX,obj.x_rs);
                % 1.1.3 Bending 
                obj.x_b = obj.bendingMoment(obj.x_ffx(:,2:3),xqXX,xMX,L,dL);
                % 1.1.4 Cutting
                obj.x_c = obj.cuttingForce(obj.x_b);
            end
            
            % 2. Reactions in the y axies
            % 2.1 Check if forces in y axies exiest
            if ~(isempty(yMX) && isempty(yFAX) && isempty(yqXX))
                % 2.1.1 Reaction in supports
                obj.y_rs = obj.reactionSupports(xS,xF,yFAX,yqXX,yMX(:,1));
                % 2.1.2 Array of forces
                obj.y_ffx = obj.reactionArray(xS,xF,yFAX,yqXX,obj.y_rs);
                % 2.1.3 Bending 
                obj.y_b = obj.bendingMoment(obj.y_ffx(:,2:3),yqXX,yMX,L,dL);
                % 2.1.4 Cutting
                obj.y_c = obj.cuttingForce(obj.y_b);
            end
            
            % 3. Sum of bending
            obj.b = sqrt(obj.x_b.^2+obj.y_b.^2);
            obj.b(2,:) = obj.y_b(2,:);
            % 3.1 Sum of forces
            fx = sqrt(obj.y_ffx(:,2).^2+obj.x_ffx(:,2).^2);
            fx(:,2) = obj.y_ffx(:,3);            
            
            % 4. Diameter of shaft from benfing
            obj.d_b = obj.bendingStressDiameter(obj.b,n,Re);
            
            % 5. Torque
            obj.t = obj.torque(dL,L,TX);
            
            % 6. Main loop of fitting shaft parameters
            d_b_temp = obj.d_b;
            while true
                % 6.1 Silhouette of shaft
                obj.d_st = obj.fitDiameterVector(d_b_temp,dS,dJ,fx,u);
                % 6.2 Diameter of shaft
                obj.d_s = obj.d_st(2,:);%%%%%%%%%%%%%%%% dodane
                obj.d_s(2,:) = obj.d_st(6,:); %%% (5:6,:)
                % 6.3 Strength indicator
                obj.d_Wg = pi*obj.d_s(1,:).^3/32;
                obj.d_W0 = pi*obj.d_s(1,:).^3/16;
                % 6.4 Geometrical inertial moment
                obj.d_I = obj.geometricInertia(obj.d_s(1,:));
                
                % 6.5 Stress from bending
                obj.sigma = obj.bendingStress(obj.b,obj.d_Wg);
                
                % 6.6 Angle of defletion
                obj.a_d = obj.deflectionAngle(obj.b,E,obj.d_I,fx); 
                % 6.7 Line of deflection
                obj.l_d = obj.deflectionLine(obj.a_d,xS);
                
                % 6.8 Stress form torque
                obj.tau = obj.torqueStress(obj.t,obj.d_W0);
                
                % 6.9 Angle of twist
                obj.a_t = obj.torqueAngle(G,obj.d_I,obj.t*10^-6);%%%%%%%%%%
                
                % 6.10 Stress from Huber Mises theory
                obj.sigma_HM = obj.theoryHuberMises(a,obj.sigma,obj.tau);
                % 6.11 Chceck safety factor
                sigma_HM_max = max(obj.sigma_HM(1,:));
                obj.sf = Re/sigma_HM_max;
                
                % 6.12 Critical rotations
                obj = obj.ciritcalVelosity();
                
                % 6.12 Conditions
                daX = max(obj.a_d(1,:));
                dlX = max(obj.l_d(1,:));
                taX = max(obj.a_t(1,:));
                if (obj.sf < n || daX > maxDA || ...
                        dlX > maxDL || ...
                        taX > maxTA)
                    d_b_temp(1,:) = d_b_temp(1,:).*1.01;
                    continue
                end

                break
            end
            
            out = obj;
        end
        
        function out = area(~,d)
            %% method "area" is used to computed
            %   the area of circle with diameter d.
            %   If parameter of method is incorrect 
            %       method returns -1.
            
            % 1. The input correction check
            if ~(isnumeric(d) && (d > 0))
                out = -1;
                return
            end      
            
            % 2. Area of circle
            out = pi*d^2/4;
        end
        
        function out = bendingContinous(~,qXX,l,dl)
            %% method "bendingContinous" is used to
            %   count bending in the axis or shaft with
            %   continous forces.
            % qXX - array of cont. forc. [[q1,x1,x2]....]
            %   q - elementary force, N/m
            %   x1 - start point, m
            %   x2 - stop point, m
            % l - length of axies or shaft, m
            % dl - step, m
            %   Result is array of bending and length [[b1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.

            % 1. The inputs correction check
            if ~(isnumeric(qXX) && ...
                    (sum(qXX(:,3)>qXX(:,2))==length(qXX(:,1))) &&...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(dl) && (dl>0))
                out = -1;
                return
            end          
            % 2. Vector of lenghts
            L = 0:dl:l;
            % 3. Length of length vector
            LLen = length(L);
            % 4. Quantity of continous forces
            FLen = length(qXX(:,1));
            % 5. Bending
            B = zeros([1 LLen]);
            for Fi = 1:FLen
                for Bi = 1:LLen
                    if L(Bi) < qXX(Fi,2)
                        continue
                    end
                    if L(Bi) < qXX(Fi,3)
                        B(Bi) = B(Bi) + qXX(Fi,1)*(L(Bi)-qXX(Fi,2))^2/2;
                    else
                        B(Bi) = B(Bi) + qXX(Fi,1)*(qXX(Fi,3)-qXX(Fi,2))*...
                            ((L(Bi)-qXX(Fi,2))/2);
                    end
                end
            end
            % 6. Out
            out = [B;L];
        end
        
        function out = bendingExternal(~,BX,l,dl)
            %% method "bedingExternal" is used to
            %   count bending moment in the axies or shaft
            %   generated by external moments.
            % BX - array of bendings and set points [[B1,x1]...]
            % l - length of axies or shaft, m
            % dl - elementary step, m
            %   Return is [[b1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(BX) && ...
                    (sum(BX(:,2)>=0)==length(BX(:,1))) &&...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(dl) && (dl>0))
                out = -1;
                return
            end         
            % 2. Vector of lenghts
            L = 0:dl:l;
            % 3. Length of length vector
            LLen = length(L);
            % 4. Quantity of bending moments
            BMLen = length(BX(:,1));
            % 5. Bending
            B = zeros([1 LLen]);
            for BMi = 1:BMLen
                for Bi = 1:LLen
                    if L(Bi) < BX(BMi,2)
                        continue
                    end
                    B(Bi) = B(Bi) + BX(BMi,1);
                end
            end
            % 6. Out
            out = [B;L];
        end
        
        function out = bendingFocused(~,FX,l,dl)
            %% method "bendingFocused" is used to
            %   counting bending in axies or shaft generated
            %   by focused forces.
            % FX - array of forces and set points of them [[F1,x1]....]
            % l - length of axies or shaft, m
            % dl - counting step, m
            %   Result is array of bending and length [[b1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(FX) && ...
                    (sum(FX(:,2)>=0)==length(FX(:,1))) &&...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(dl) && (dl>0))
                out = -1;
                return
            end  
            % 2. Vector of lenghts
            L = 0:dl:l;
            % 3. Length of length vector
            LLen = length(L);
            % 4. Quantity of focused forces
            FLen = length(FX(:,1));
            % 5. Bending
            B = zeros([1 LLen]);
            for Fi = 1:FLen
                for Bi = 1:LLen
                    if L(Bi) < FX(Fi,2)
                        continue
                    end
                    B(Bi) = B(Bi) + FX(Fi,1)*(L(Bi)-FX(Fi,2));
                end
            end
            % 6. Out
            out = [B;L];
        end
        
        function out = bendingMoment(obj,FX,qXX,BX,l,dl)
            %% method "bendingMoment" is used to 
            %   complrx count bendings in the axies or shatf.
            % FX - array of focused forces [[F1,x1]...]
            % qXX - array of continous forces [[q1,X11,X12]...]
            % BX - array of external bending moments [[b1,x1]...]
            % l - length of axies or shaft, m
            % dl - counting step, m
            %   Result is array of bending and length [[b1,l1]...]
            %
            % To skip parameter put []
            %
            %   If parameters of method are incorrect 
            %       method returns -1.

            % 1. The inputs correction check
            if ~(isnumeric(l) && (l>0) && ...
                    isnumeric(dl) && (dl>0))
                out = -1;
                return
            end  
            % 2. Bending moment from focused forc.
            f = [];
            if ~isempty(FX)
                f = obj.bendingFocused(FX,l,dl);
            end
            % 3. Bending moment from continous forc.
            c = [];
            if ~isempty(qXX)
                c = obj.bendingContinous(qXX,l,dl);
            end
            % 4. Bending moment from external moments
            e = [];
            if ~isempty(BX)
                e = obj.bendingExternal(BX,l,dl);
            end
            % 5. Sum of bendings
            B = zeros([2 length(0:dl:l)]);
            if ~isempty(f)
                B(1,:) = B(1,:) + f(1,:);
                B(2,:) = f(2,:);
            end
             if ~isempty(c)
                B(1,:) = B(1,:) + c(1,:);
                B(2,:) = c(2,:);
            end           
            if ~isempty(e)
                B(1,:) = B(1,:) + e(1,:);
                B(2,:) = e(2,:);
            end            
            % 5. out
            out = B;
        end
        
        function out = bendingStress(~,BL,W)
            %% mthod "bendingStress" is used to
            %   compute stress in the cross section
            %   of axies or shaft charged by bending 
            %   moment.
            % BL - array of bending moment [[b1,l1]...]
            % W - vector of flexural modulus [w1,...]
            %   w - flexural modulus in the free axis
            %       foursome: W = bh^2/6
            %       triangle: W = bh^2/24
            %       circle: W = pi d^3/32
            %       ring: W = pi(D^4-d^4)/32D
            %   Return is an array of sterss [[s1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(BL) && ...
                    isnumeric(W))
                out = -1;
                return
            end  
            % 2. Quantity of corss sections
            WXLen = length(W);
            % 3. Stress
            S = zeros([2 WXLen]);            
            for WXi = 1:WXLen
                S(1,WXi) = BL(1,WXi)/W(WXi);
            end
            % 4. out
            S(1,:) = S(1,:);
            S(2,:) = BL(2,:);
            out = S;
        end
        
        function out = bendingStressDiameter(~,BL,n,Re)
            %% method "bendingStressDiameter" is used to
            %   compute minimal diameter of circle cross
            %   section of the sxies or shaft with bending moment.
            % BL - array of bending moment [[b1,l1]...]
            % n - safety factor
            % Re - yeld strength, Pa
            %   Result is an array of diameters [[d1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(BL) && ...
                    isnumeric(n) && (n>0) &&...
                    isnumeric(Re) && (Re>0))
                out = -1;
                return
            end  
            % 2. Length of bending vector
            BLLen = length(BL(1,:));
            % 3. Diameter
            D = [2 BLLen];
            for BLi = 1:BLLen
                D(1,BLi) = (32*n*abs(BL(1,BLi))/pi/Re)^(1/3);
            end
            % 4. Out
            D(2,:) = BL(2,:);
            out = D;            
        end
        
        function out = cuttingForce(~,BL)
            %% method "cuttingForce" is used to
            %   count cutting force in the axis or shaft
            %   from bending moment.
            % BM = array of bending moment [[B1,l1]...]
            %   Result is [[C1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.

            % 1. The inputs correction check
            if ~isnumeric(BL)
                out = -1;
                return
            end     
            % 2. Incrementation of length
            dl = BL(2,2)-BL(2,1);
            % 3. Quantity of points
            BLLen = length(BL(1,:));
            % 4. Cutting force
            C = zeros([1 BLLen]);
            for Ci = 1:BLLen-1
                C(Ci) = (BL(1,Ci+1)-BL(1,Ci))/dl;
            end
            C(BLLen) = C(Ci);
            % 5. Simple PickCleaner
            if ((C(2) == C(3)) &&...
                    C(1) ~= C(2))
                C(1) = C(2);
            end
            if ((C(BLLen-2) == C(BLLen-3)) &&...
                    C(BLLen) ~= C(BLLen-2))
                C(BLLen) = C(BLLen-2);
            end
            for Ci = 2:BLLen-1
                if C(Ci-1) <= C(Ci+1)
                    c = C(Ci+1)/C(Ci-1)-1;
                else
                    c = C(Ci-1)/C(Ci+1)-1;
                end
                % float force
                if c <= .0005
                    if C(Ci) ~= C(Ci+1)
                        C(Ci) = C(Ci+1);
                    end
                end
            end
%                 else
%                     if Ci > 2
%                         if C(Ci) ~= C(Ci+1)
%                             dC = C(Ci+1)-C(Ci);
%                             if C(Ci-2) == C(Ci)-2*dC
%                                 C(Ci-1) = C(Ci)-dC;
%                             end
%                         end
%                     end
%                 end
%             end
            % 6. out
            out = [C;BL(2,:)];
        end
        
        function obj = ciritcalVelosity(obj)
            obj.w_kr = sqrt(9.81./obj.l_d(1,:));
            obj.w_kr(2,: )= obj.l_d(2,:);
        end
        
        function out = deflectionAngle(~,BL,E,J,FX)
            %% method "deflectionAngle" is used to
            %   compute angle of deflection in axis or shaft
            %   with bending moment.
            % BL - array of bending moment [[b1,l1]...]
            % E - Young's modulus, Pa
            % J - array of interia moment [j1,...]
            % FX - array of focused forces, [[f1,x1]...]
            %   Return is an array of angle in radians [[a1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.

            % 1. The inputs correction check
            if ~(isnumeric(BL) && ...
                    isnumeric(E) && (E>0) && ...
                    isnumeric(J) &&...
                    isnumeric(FX))
                out = -1;
                return
            end           
            % 2. Length of bending
            BLLen = length(BL(1,:));
            % 3. Incrementation of length
            dl = BL(2,2)-BL(2,1);
            % 4. Angle
            A = zeros([2 BLLen]);
            for BLi = 2:BLLen
                A(1,BLi) = A(1,BLi-1) - BL(1,BLi)*dl/E/J(BLi);
            end
            A(1,:) = atan(A(1,:));
            % 5. Max force
            [~, FXMax] = max(abs(FX(:,1)));
            xMax = FX(FXMax,2);
            Li = find(BL(2,:)==xMax);
            % 6. Angle correction
            dA = A(1,Li);
            A(1,:) = A(1,:) - dA;
            % 7. Out
            A(2,:) = BL(2,:);
            out = A;
        end
        
        function out = deflectionLine(~,DA,xS)
            %% method "deflectionLine" is used to
            %   compute deflection line in the axies or shaft.
            % DA - array of deflaction angle, [[a1,l1]...]
            % xS - set point of static support
            %   Return is an array of line in meters [[L1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.            
            
            % 1. The inputs correction check
            if ~(isnumeric(DA) && ...
                    isnumeric(xS))
                out = -1;
                return
            end             
            % 2. Length of deflection
            DALen = length(DA(1,:));
            % 3. Incrementation of length
            dl = DA(2,2)-DA(2,1);
            % 4. Line
            L = zeros([2 DALen]);
            for DAi = 2:DALen
                L(1,DAi) = L(1,DAi-1)-DA(1,DAi)*dl;
            end
            % 5. Line correction
            xSi = find(DA(2,:)==xS);
            dL = min(L(1,:));%L(1,xSi);
            L(1,:) = L(1,:)-dL;
            % 5. Out
            L(2,:) = DA(2,:);
            out = L;
        end
        
        function out = fitDiameter(~,d,dVec)
            %% method "fitDiameter" is used to fit d
            %   parameter to first bigger diameter from dVec
            %   vector. 
            %   If parameters of method are incorrect 
            %       method returns -1.
            %   Values in the dVec has to be rising.
            % [d] = float
            % [dVec] = [float]
                
            % 1. The inputs correction check
            if ~(isnumeric(d) && (d>0) &&...
                    isnumeric(dVec))
                out = -1;
                return
            end
            % 2. Length of dVec
            dVecLen = length(dVec);
            % 3. Fitting loop
            for di = 1:dVecLen
                if dVec(di) > d
                    out = dVec(di);
                    return
                end
            end
            % 4. Oversize
            out = -1;
        end
        
        function out = fitDiameterVector(obj,DL,SD,JD,FX,u)
            %% method "fitDiameterVector" is used to
            %   fit diameter of axies or shaft to standard 
            %   diameters.
            % DL - vector of diameters, [[d1,l1]...]
            % SD - standard diameter of shaft, m
            % JD - standard diameter of journal, m
            % FX - forces on the aies or shaft, [[f1,x1]...]
            % u - area of force impact factor
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The input correction check
            if ~(isnumeric(DL) && ...
                    isnumeric(SD) &&...
                    isnumeric(JD) &&...
                    isnumeric(FX))
                out = -1;
                return
            end
            % 2. Quantity of forces
            FXLen = length(FX(:,1));
            [~, FXMax] =  max(abs(FX(:,1)));
            FXMaxL = FX(FXMax,2);
            % 3. Points in d vector
            DLLen = length(DL(1,:));
            [~, DLMax] = max(DL(1,:));
            DLMaxL = DL(2,DLMax);
            % 4. Check compatibility
            if FXMaxL ~= DLMaxL
                i = find(DL(2,:)==FXMaxL);
                DL(1,i) = 1.2*DL(1,DLMax);
                DLMax = i;
            end
            % 5. Diameters
            D = zeros([6 DLLen]);
            % 5.1 Linear approximation
            Dl1 = obj.linearDiameter(DL(:,1:DLMax));
            Dl2 = obj.linearDiameter(DL(:,DLMax:DLLen));
            D(1,1:DLMax) =DL(2,1:DLMax)*Dl1(1)+Dl1(2);
            D(1,DLMax:DLLen) = DL(2,DLMax:DLLen)*Dl2(1)+Dl2(2);
            % 5.2 Fitting diameters to standard values
            for Di = 1:DLLen
                D(2,Di) = obj.fitDiameter(D(1,Di),SD);
            end
            % 5.3 Fitting std d. to journal diameters
            for Di = 1:DLLen
                D(3,Di) = obj.fitDiameter(D(1,Di),JD);
            end
            % 6. Areas influented by forces
            L =DL(2,DLLen);
            XX = [0 L*.05];
            for XXi = 1:FXLen
                if (FX(XXi,2) < L*.03) ||...
                        (FX(XXi,2) > L*.95)
                    continue
                end
                x = FX(XXi,2);
                xl = find(DL(2,:)==x);
                xd = u*D(3,xl);
                XX(XXi,:) = [x-xd x+xd];
            end
            %if XX(XXi,1) == 0
                XX(XXi+1,:) = [L*.95 L];%%%%%%%%,
            %else
            %    XX(XXi+1,:) = [L*.95 L];
            %end
            XXMax = FXMax;
            XXLen = length(XX(:,1));
            % 7. Selecting diameters of journals
            for Di = 1:DLLen
                if ~obj.isInfluented(DL(2,Di),XX)
                    D(3,Di) = 0;
                end
            end
            % 8. Diameters of journals
            dXX = zeros([XXLen 3]);
            dXX(:,2:3) = XX;
            for XXi = 1:XXLen
                if XXi <= XXMax
                    x = dXX(XXi,3);
                    xl = find(DL(2,:)>=x);
                    dXX(XXi,1) = D(3,xl(1)-1); %%%%%%%%%%%%%% -1
                else
                    x = dXX(XXi,2);
                    xl = find(DL(2,:)>=x);
                    dXX(XXi,1) = D(3,xl(1));                     
                end
            end
            % 9. Correction of diameters
            for XXi = 2:XXMax
                while obj.fitDiameter(dXX(XXi-1,1),JD) >= dXX(XXi,1)
                    dXX(XXi,1) = obj.fitDiameter(dXX(XXi,1),JD);
                    dXX(XXi,1) = obj.fitDiameter(dXX(XXi,1),JD);
                end
            end
            for XXi = XXLen-1:-1:XXMax
                while obj.fitDiameter(dXX(XXi+1,1),JD) >= dXX(XXi,1)
                    dXX(XXi,1) = obj.fitDiameter(dXX(XXi,1),JD);
                    dXX(XXi,1) = obj.fitDiameter(dXX(XXi,1),JD);
                end
            end            
            % 10. Redimension journals
            for XXi = 1:XXLen
                for Di = 1:DLLen
                    if (DL(2,Di)>=dXX(XXi,2)) &&...
                            (DL(2,Di)<=dXX(XXi,3))
                        D(3,Di) = dXX(XXi,1);
                    end
                end
            end
            % 11. Fitting standard dimensions
            sdXX = [];
            sdi = 1;
            for XXi = 1:XXMax-1
                % 11.1 Quantity of iteration
                d0 = dXX(XXi,1);
                d1 = dXX(XXi+1,1);
                dn = obj.fitDiameter(d0,SD); 
                dni = 1;
                while obj.fitDiameter(dn,SD) < d1
                    dn = obj.fitDiameter(dn,SD);
                    dni = dni + 1;
                end
                % 11.2 Distance between journals
                l = dXX(XXi+1,2)-dXX(XXi,3);
                dl = l/dni;
                % 11.3 Standard array
                dn = d0; 
                for sdXXi = 1:dni
                    dn = obj.fitDiameter(dn,SD);
                    sdXX(sdi,1) = dn;
                    sdXX(sdi,2) = dXX(XXi,3)+dl*(sdXXi-1);
                    sdXX(sdi,3) = dXX(XXi,3)+dl*sdXXi;
                    sdi = sdi+1;
                end
            end
            for XXi = XXLen:-1:XXMax+1
                % 11.1 Quantity of iteration
                d0 = dXX(XXi,1);
                d1 = dXX(XXi-1,1);
                if (XXi-1) == XXMax
                    d1 = obj.fitDiameter(dXX(XXi-1,1),JD);
                    d1 = obj.fitDiameter(d1,JD);
                end
                dn = obj.fitDiameter(d0,SD); 
                dni = 1;
                while obj.fitDiameter(dn,SD) < d1
                    dn = obj.fitDiameter(dn,SD);
                    dni = dni + 1;
                end
                % 11.2 Distance between journals
                l = dXX(XXi,2)-dXX(XXi-1,3);
                dl = l/dni;
                % 11.3 Standard array
                dn = d0; 
                for sdXXi = 1:dni
                    dn = obj.fitDiameter(dn,SD);
                    sdXX(sdi,1) = dn;
                    sdXX(sdi,3) = dXX(XXi,2)-dl*(sdXXi-1);
                    sdXX(sdi,2) = dXX(XXi,2)-dl*sdXXi;
                    sdi = sdi+1;
                end
            end
            sdXXLen = length(sdXX(:,1));
            % 12. Redimension standard
            for XXi = 1:sdXXLen
                for Di = 1:DLLen
                    if (DL(2,Di)>sdXX(XXi,2)) &&...
                            (DL(2,Di)<sdXX(XXi,3))
                        D(4,Di) =sdXX(XXi,1);
                    end
                end
            end
            % 13. Sum dimensions
            D(5,:) = D(3,:)+D(4,:);
            % 14. Zero elimination
            for Di = 2:DLLen-1
                if D(5,Di) == 0
                    D(5,Di) = D(5,Di+1);
                end
            end
            % out
            D(6,:) = DL(2,:);
            out = D;
        end
        
        function out = geometricInertia(~,d)
            %% method "geometicInertia" is used to
            %   compute the geometrical inertial moment
            %   of the solids of revolution.
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The input correction check
            if ~isnumeric(d)
                out = -1;
                return
            end   
            %2. Inertial computing
            out = pi*d.^4/64;
        end
        
        function out = radiusOfInertia(~,d)
            %% method "radiusOfInertia" is used to
            %   compute the radius of the geometrical inertia
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
     
            % 1. The input correction check
            if ~(isnumeric(d) && (d>0))
                out = -1;
                return
            end   
            % 2. Radius
            out = d/4;
        end
        
        function out = reactionArray(obj,xS,xF,FAX,qXX,FS)
            %% method "ractionArray" is used to sort
            %   all type forces in the axies or shaft by set point.
            % xS - set point of static support, m
            % xF - set point of floating support, m
            % FAX - array of focused forces [[f1,a1,x1]...]
            %   f - force, N
            %   a - clockwise angle from horizontal axis, rad
            %   x - set point, m
            % qXX - array of continous forces [[q1,x11,x12]...]
            %   q - elementary force, N/m
            %   x1 - start set point, m
            %   x2 - stop set point, m
            % FS - vector of reaction in supports [Fsx,Fsy,Ffy]
            %   
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The input correction check
            if ~(isnumeric(xS) && (xS>=0) &&...
                    isnumeric(xF) && (xF>=0)) 
                out = -1;
                return
            end   
            if ~isempty(FAX)
                if ~isnumeric(FAX)
                    out = -1;
                    return
                end
            end
            if ~isempty(qXX)
                if ~isnumeric(qXX)
                    out = -1;
                    return
                end
            end
            if ~isempty(FS)
                if ~isnumeric(FS)
                    out = -1;
                    return
                end
            end
            % 2. Array of focused forces
            FFX = [];
            if ~isempty(FAX)
                FFX = obj.reactionSpread(FAX(:,1:2));
                FFX(:,3) = FAX(:,3);
            end
            % 3. Array of continous forces
            FX = [];
            if ~isempty(qXX)
                FX = zeros([length(qXX(:,1)) 3]);
                FX(:,2:3) = obj.reactionContinous2Focus(qXX);
            end      
             % 4. Array of reactions in supports
             FFL = [[FS(1) FS(2) xS];...
                 [0 FS(3) xF]];
             % 5. Array of forces
             FFArray = [FFX;FX;FFL];
             out = sortrows(FFArray,3);
        end
        
        function out = reactionBendingMoment(~,xS,xF,FX,M)
            %% method "reactionBendingMoment" is used to
            %   compute banding moment in the axis or shaft
            %   and raection in floating support.
            % xS - point where is set static support, m
            % xF - set point floating support, m
            % FX - array of forces and points [[F1,x1]...]
            % M - bendings moments [M1,...]
            %   Return is [Ms, Ff]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The input correction check
            if ~(isnumeric(xS) && (xS>=0) &&...
                    isnumeric(xF) && (xF>=0)) 
                out = -1;
                return
            end 
            if ~isempty(FX)
                if ~isnumeric(FX)
                    out = -1;
                    return
                end
            end
            if ~isempty(M)
                if ~isnumeric(M)
                    out = -1;
                    return
                end
            end
            % 2. Quantity of points
            FXLen = length(FX(:,1));
            % 3. Computing moments
            out = 0;
            for FXi = 1:FXLen
                l = xS - FX(FXi,2);
                out = out + FX(FXi,1)*l;
            end
            % 4. Adding bending moments
            if ~isempty(M)
                out = out + sum(M);
            end
            % 5. Reaction in floating support
            l = xF - xS;
            out(2) = out(1)/l;
        end
        
        function out = reactionContinous2Focus(~,qXX)
            %% method "reactionContinous2Focus" is used to
            %   conversion continous force to focused force/
            % q - elementary force, N/m
            % x1 - start point
            % x2 - stop point
            %   Result is [F, x]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
             % 1. The inputs correction check
             if ~(isnumeric(qXX) &&...
                     sum(qXX(:,3)>qXX(:,2))==length(qXX(:,1)))
                 out = -1;
                 return
             end
            % 2. Length of forcing
            l = qXX(:,3)-qXX(:,2);
            % 3. Focused force
            F = l.*qXX(:,1);
            % 4. Result
            out = F;
            out(:,2) = qXX(:,2)+l/2;
        end
        
        function out = reactionSpread(~,FA)
            %% method "reactionSpread" is used to
            %   spread the resultant force. Horizontal
            %   axis is base to count angle. 
            % F - force, N
            % a - angle, rad
            %   Result is [Fx, Fy]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(FA))
                out = -1;
                return
            end   
            % 2. Spreading
            out = FA(:,1).*sin(FA(:,2)); 
            out(:,2) = FA(:,1).*cos(FA(:,2));
        end
        
        function out = reactionSupports(obj,xS,xF,FAX,qXX,M)
            %% method "reactionSupports" is used to
            %   appoint reacts in supports of axis or shaft.
            % xS - set point static support
            % xF - set point floating support
            % FAX - array of focused forces [[F1,a1,x1]....]
            %   F - resultant force, N
            %   x - set point, m
            %   a - angle btw. horizontal and force, rad
            %       positive in clockwise
            % qXX - array of continous forces [[F1,x11,x12]....]
            %   q - elementary force, N/m
            %   x1 - start point, m
            %   x2 - stop point, m
            % M - array of bending moments [M1,...]
            %
            % To skip force array put "[]"
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(xS) && (xS>=0) &&...
                    isnumeric(xF) && (xF>=0))
                out = -1;
                return
            end
            % 2. Focused forces spreading
            FFX = [];
            if ~isempty(FAX)
                FFX(:,1:2) = obj.reactionSpread(FAX(:,1:2));
                FFX(:,3) = FAX(:,3);
            end
            % 3. Continoused forces converting
            FX = [];
            if ~isempty(qXX) 
                FX(:,2:3) = obj.reactionContinous2Focus(qXX);
            end
            % 4. Total array of forces
            FXArr = [FFX;FX];
            % 5. Horizontal reaction in static support
            Fs_x = -sum(FXArr(:,1));
            % 6. Bending moment in the axis or shaft
            BM = obj.reactionBendingMoment(xS,xF,FXArr(:,2:3),M);
            % 7. Vertical reaction in floating support
            Ff_y = BM(2);
            % 8. Vertical raction in static support
            Fs_y = -sum(FXArr(:,2))-Ff_y;
            % 9. Result
            out = [Fs_x,Fs_y,Ff_y];
        end
        
        function out = slimness(obj,d,k,l)
            %% method "slimness" is used to
            %   compute slimness of axis.
            % d = diameter
            % l = length
            % k = set coefficient
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(d) && (d>0) && ...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(k) && (k>0))
                out = -1;
                return
            end         
            % 2. Slimness
            out = l*k/obj.radiusOfInertia(d);
        end
        
        function out = slimnessCondition(obj,d,E,k,l,sigma_H)
            %% method "slimnessCondition" is used to
            %   compare slimness of axis or shaft to critical 
            %   slimness of material
            % d - diameter, m
            % E - Young's modulus, Pa
            % k - set coefficient
            % l - length, m
            % sigma_H - limit of proportionality, Pa
            %
            %   If parameters of method are incorrect 
            %       method returns -1. 
            
            s = obj.slimness(d,k,l);
            sc = obj.slimnessCritical(E,sigma_H);
            % 1. The inputs correction check
            if (s == -1 || sc == -1)
                out = -1;
                return
            end
            %2. Compare values
            out = (s >= sc);
        end
        
        function out = slimnessCritical(~,E,sigma_H)
            %% method "slimnessCritical" is used to
            % compute critical slimness for material.
            %   E - Young's modulus, Pa
            %   sigma_H - limit of proportional, Pa
            %
            %   If parameters of method are incorrect 
            %       method returns -1.      
            
            % 1. The inputs correction check
            if ~(isnumeric(E) && (E>0) && ...
                    isnumeric(sigma_H) && ...
                    (sigma_H>0))
                out = -1;
                return
            end        
            % 2. Critical slimness
            out = pi*sqrt(E/sigma_H);
        end
        
        function out = speedCritical(~,DL,g)
            %% method "speedCritical" is used to
            %   count critical rotation speed of shaft.
            % DL -deflaction line array, [[d1,l1]...]
            % g - gravity accelleration
            %   Return is in rad/s
            %
            %   If parameters of method are incorrect 
            %       method returns -1.      
            
            % 1. The inputs correction check
            if ~(isnumeric(DL) &&  ...
                    isnumeric(g) && (g>0))
                out = -1;
                return
            end  
            % 2. Max deflaction
            DMax = max(DL(1,:));
            % 3. Critical speed
            out = sqrt(g/DMax);
        end
        
        function out = sterssEuler(~,E,Lambda)
            %% method "sterssEuler" is used to
            %   compute the value of stress in the axis or 
            %   shaft from Euler stress thory
            %
            %   If parameters of method are incorrect 
            %       method returns -1.      
            
            % 1. The inputs correction check
            if ~(isnumeric(E) && (E>0) && ...
                    (Lambda>0))
                out = -1;
                return
            end      
            % 2. Euler stress
            out = pi^2*E/Lambda^2;
        end
        
        function out = sterssCriticalEulerDiameter(~,E,F,k,l,n)
            %% method "sterssCriticalEulerDiameter" is used to
            %   compute minimal diameter of axies or shaft from 
            %   material.
            % E - Young's modulus, Pa
            % F - force, N
            % k - set coefficient
            % l - length, m
            % n - safety factor
            %
            %   If parameters of method are incorrect 
            %       method returns -1. 
            
            % 1. The inputs correction check
            if ~(isnumeric(E) && (E>0) && ...
                    isnumeric(F) && (F>0) && ...
                    isnumeric(k) && (k>0) && ...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(n) && (n>0))
                out = -1;
                return
            end            
            % 2. Diameter
            out = (64*F*n*(k*l)^2/pi^3/E)^(1/4);
        end
        
        function out = stressJohnsonOstenfeld(~,E,Lambda,Re)
            %% method "stressJohnsonOstenfeld" is used to 
            %   compute sress in the axis or shaft from 
            %   Johnson-Ostenfeld parabole
            % E - Young's modulus
            % Lambda - slimness coefficient
            % Re - yield strength
            %
            %   If parameters of method are incorrect 
            %       method returns -1.   
            
            % 1. The inputs correction check
            if ~(isnumeric(E) && (E>0) && ...
                    isnumeric(Lambda) && ...
                    (Lambda>0) && ...
                    isnumeric(Re) && (Re>0))
                out = -1;
                return
            end    
            % 2. J.O. stress
            out = Re-Re^2*Lambda^2/4/E/pi^2;
        end
        
        function out = stressCriticalJohnsonOstenfeldDiameter(~,E,F,k,l,n,Re)
            %% method "stressCriticalJohnsonOstenfeldDiameter" is used to
            %   compute minimal diameter of axis or shaft form material.
            % E - Young's modulus, Pa
            % F - force, N
            % k - set coefficient
            % l - length, m
            % n - safety factor
            % Re - yield strength
            %
            %   If parameters of method are incorrect 
            %       method returns -1. 
            
            % 1. The inputs correction check
            if ~(isnumeric(E) && (E>0) && ...
                    isnumeric(F) && (F>0) && ...
                    isnumeric(k) && (k>0) && ...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(n) && (n>0) &&...
                    isnumeric(Re) && (Re>0))
                out = -1;
                return
            end    
            % 2. Coefficients of the square equation
            a = pi*Re^2*(k*l)^2;
            b = -64*Re*pi*E;
            c = 256*F*n*E*pi^2;
            % 3. Discriminant 
            Delta = b^2-4*a*c;
            % 4. Roots
            R1 = sqrt((-b+sqrt(Delta))/2/a);
            R2 = sqrt((-b-sqrt(Delta))/2/a);
            out = [R1 R2];
        end
        
        function out = theoryHuberMises(~,a,BL,TL)
            %% method "thetoryHiberMises" is used to
            %   count relative stress in the shaft.
            % a - coefficient
            % BL - bending array
            % TL - torque array
            %
            %   If parameters of method are incorrect 
            %       method returns -1. 
            
            % 1. The inputs correction check
            if ~(isnumeric(a) && ...
                    isnumeric(BL) && ...
                    isnumeric(TL))
                out = -1;
                return
            end 
            % 2. Quantity of points
            BLLen = length(BL(1,:));
            TLLen = length(TL(1,:));
            if BLLen ~= TLLen
               out = -1;
               return
            end
            % 3. Max values
            BMax = max(BL(1,:));
            TMax = max(TL(1,:));
            % 4. Comparation
            BgT = BMax >TMax;
            % 5. Relative
            R = zeros([2 BLLen]);
            if BgT
                R(1,:) = sqrt((BL(1,:)/a).^2+TL(1,:).^2);
            else
                R(1,:) = sqrt(BL(1,:).^2+(TL(1,:)*a).^2);
            end
            % 6. Out
            R(2,:) = BL(2,:);
            out = R;
        end        
        
        function out = torque(~,dl,l,TX)
            %% method "torque" is used to
            %   compute torques in axies or shaft
            % dl - step, m
            % l - length of axies or shaft, m
            % TX - focused torque array [[t1,x1]...]
            %   Return is an array of torque [[t1,l1]...];
            %
            %   If parameters of method are incorrect 
            %       method returns -1. 
            
            % 1. The inputs correction check
            if ~(isnumeric(dl) && (dl>0) && ...
                    isnumeric(l) && (l>0) && ...
                    isnumeric(TX))
                out = -1;
                return
            end 
            % 2. Length vector
            L = 0:dl:l;
            LLen = length(L);
            % 3. Quantity of torques
            TXLen = length(TX(:,1));
            % 4. Torque
            T = zeros([2 LLen]);
            for TXi = 1:TXLen
                for Li = 1:LLen
                    if L(Li) >= TX(TXi,2)
                        T(1,Li) = T(1,Li) + TX(TXi,1);
                    end
                end
            end
            % 5. Out
            T(2,:) = L;
            out = T;
        end
        
        function out = torqueAngle(~,G,I,TL)
            %% method "torqueAngle" is used to
            %   compute angle of twist in shaft.
            % G - modulus of the lateral elasticy, Pa
            % I - inertia vector [i1,...], m^4
            % TL - torque array [[t1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1. 
            
            % 1. The inputs correction check
            if ~(isnumeric(G) && (G>0) && ...
                    isnumeric(I) && ...
                    isnumeric(TL))
                out = -1;
                return
            end 
            % 2. Length incrementation
            dl = TL(2,2) - TL(2,1);
            % 3. Quantity of points
            TLLen = length(TL(2,:));
            ILen = length(I);
            if TLLen ~= ILen
                out = -1;
                return
            end
            % 4. Twisting
            fi = zeros([2 TLLen]);
            for TLi = 2:TLLen
                fi(1,TLi) = fi(1,TLi-1) + TL(1,TLi)*dl/G/I(TLi)*10^3; 
            end
            % 5. Out
            fi(2,:) = TL(2,:);
            out = fi;
        end
        
        function out = torqueStress(~,TL,W)
            %% mthod "torqueStress" is used to
            %   compute stress in the cross section
            %   of axies or shaft charged by bending 
            %   moment.
            % TL - array of torque [[b1,l1]...]
            % W - vector of flexural modulus [w1,...]
            %   w - flexural modulus in the free axis
            %       foursome: W = bh^2/6
            %       triangle: W = bh^2/24
            %       circle: W = pi d^3/32
            %       ring: W = pi(D^4-d^4)/32D
            %   Return is an array of sterss [[s1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(TL) && ...
                    isnumeric(W))
                out = -1;
                return
            end 
            % 2. Quantity of points
            TLLen = length(TL(1,:));
            % 3. Stress
            S = zeros([2 TLLen]);
            for TLi = 1:TLLen
                S(1,TLi) = TL(1,TLi)/W(TLi);
            end
            % 4. Out
            S(1,:) = S(1,:);
            S(2,:) = TL(2,:);
            out = S;
        end        
        
        function out = torqueStressDiameter(~,n,Re,TL)
            %% method "torqueStressDiameter" is used to
            %   count diameter of circle shaft
            % n - safety factor,
            % Re - yield strength, Pa
            % TL - array of torque [[t1,l1]...]
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(n) && (n>0) && ...
                    isnumeric(Re) && (Re>0) && ...
                    isnumeric(TL))
                out = -1;
                return
            end 
            % 2. Quantity of length of shaft
            TLLen = length(TL(1,:));
            % 3. Diameters
            D = zeros([2 TLLen]);
            for TLi = 1:TLLen
                D(1,TLi) = (n*32*abs(TL(1,TLi))/...
                    pi/Re)^(1/3);
            end
            % 4. Out
            D(2,:) = TL(2,:);
            out = D;
        end
        
        function out = volume(obj,DL)
            %% method "volume" is used to computed
            %   the volume of axies or shaft even thought
            %   diameter of them is changing.
            %
            %   Subtraction of the volume is done by
            %       negative value of length (L) parameter.
            %   Diameter has to be allways positive.
            %
            %   If parameters of method are incorrect
            %       method returns -1.
            % [DLArray] = [[d_1,l_1],[d_2,l_2],...]
            
            % 1. The inputs correction check
            if ~isnumeric(DL)
                out = -1;
                return
            end
            % 2. Length of vector
            DLLen = length(DL(1,:));
            % 3. Step
            dl = DL(2,2) - DL(2,1);
            % 4. Sum of volumes
            out = 0;
            for dli = 1:DLLen
                out = out +obj.area(DL(1,dli))*dl;
            end
        end
        
        function out = weight(~,rho,V)
            %% method "weight" is used to
            %   compute weight of axies or shaft
            %
            %   If parameters of method are incorrect
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~(isnumeric(rho) && (rho>0) &&...
                    isnumeric(V) && (V>0))
                out = -1;
                return
            end
            % 2. Out
            out =V*rho;
        end
    end
    
    methods (Access = private)
        function out = linearDiameter(~,DL)
            %% method "linearDiameter" id used to
            %   find linear function above diameters.
            % DL - array of diameters [[d1,l1]...]
            %   Return id [A B] from y=Ax+B
            %
            %   If parameters of method are incorrect 
            %       method returns -1.
            
            % 1. The inputs correction check
            if ~isnumeric(DL) 
                out = -1;
                return
            end   
            % 2. Length of DL
            DLLen = length(DL(1,:));
            % 3. A parameter
            A0 = (DL(1,DLLen)-DL(1,1))/...
                (DL(2,DLLen)-DL(2,1));
            % 4. B parameter
            if A0 >= 0
                B0 = DL(1,DLLen)-A0*DL(2,DLLen);
            else
                B0 = DL(1,1)-A0*DL(2,1);
            end
            % 5. Check
            A = A0;
            B = B0;
            DLi = 1;
            while DLi < DLLen
                if DL(1,DLi) > (A*DL(2,DLi)+B)
                    A = A-A0/1000;
                    if A >= 0
                        B = DL(1,DLLen)-A*DL(2,DLLen);
                    else
                        B = DL(1,1)-A*DL(2,1);
                    end
                    DLi = 1;
                    continue
                end
                DLi = DLi+1;
            end
            % 6. Out
            out =[A B];
        end
        
        function out = isInfluented(~,x,XX)
            %% method "isInfluented" is used to
            %   check if x is in XX range
            % x - parameter
            % XX - array of ranges [[x10,x11]...]
            %   Result is binary
            %
            % If inputs are incorrect 
            %   method returns false.
            
            % 1. The inputs correction check
            if ~(isnumeric(x) &&...
                    isnumeric(XX))
                out = false;
                return
            end  
            % 2. Check 
            x0 = find(XX(:,1)<=x);
            x1 = find(XX(:,2)>=x);
            % 3. Out
            out = false;
            if isempty(x0) || isempty(x1)
                return
            end
            x0Len = length(x0);
            x1Len = length(x1);
            for x0i = 1:x0Len
                for x1i = 1:x1Len
                    if x0(x0i) == x1(x1i)
                        out = true;
                        return
                    end
                end
            end
        end
    end
end