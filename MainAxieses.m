close all
clear

fSize = 30;      % wielkosc liter
lSize = 2;         % gr. linii

% 1. Parametry
a = 3;
dL = 0.001;
E = 21*10^10;
G = 85*10^9;
maxDA = 100;
maxDL = 0.01;
maxTA = 100;
n = 2.5;
Re = 275*10^6;
u = 1.5;

% 2. Obiekt
AAS = AxisesAndShafts;
% 3. �rednice
dJ = AAS.dJournal_I;
dS = AAS.dShafts_I;

%% Wa� I
L = .4;
xF = L;
xS = .1;

xX = 0;
xX2 = .25;

% 2. Si�y
% 2.1 X
xMX = [0,0];
xFAX = [[1918,0,xX];
    [1226,0,xX2]];
xqXX = [0,0,.1];
% 2.2 Y
yMX = [0,0];
yFAX = [[0,0,xX];
    [5272,0,xX2]];
yqXX = [0,0,.1];
% 2.3 Skr�cajace
TX = [131,0];

% 3. Program
o1 = AAS.auto(a,dJ,dL,dS,E,G,L,maxDA,maxDL,maxTA,n,xMX,xFAX,xqXX,yMX,yFAX,yqXX,Re,TX,u,xF,xS);

%% Wa� II
L = .5;
xF = L;
xS = 0;
xX1 = .2;
xX2 = .4;
% 2. Si�y
% 2.1 X
xMX = [0,0];
xFAX = [[1919,0,xX1];
    [3209,pi/2-deg2rad(49),xX2]];
xqXX = [0,0,.1];
% 2.2 Y
yMX = [0,0];
yFAX = [[5272,0,xX1];
    [2109,pi/2,xX2]];
yqXX = [0,0,.1];
% 2.3 Skr�cajace
TX = [395,xX1];
% 3. Program
o2 = AAS.auto(a,dJ,dL,dS,E,G,L,maxDA,maxDL,maxTA,n,xMX,xFAX,xqXX,yMX,yFAX,yqXX,Re,TX,u,xF,xS);

%% Wa� III
L = .5;
xF = .47;
xS = 0.15;

xX = .3;
xX2 = L;

% 2. Si�y
% 2.1 X
xMX = [0,0];
xFAX =[[4897,pi/2-deg2rad(65),xX];
    [2500,0,xX2]];
xqXX = [0,0,.1];
% 2.2 Y
yMX = [0,0];
yFAX = [[12218,pi/2-deg2rad(78),xX];
    [0,0,xX2]];
yqXX = [0,0,.1];
% 2.3 Skr�cajace
TX = [2767,xX];
% 3. Program
o3 = AAS.auto(a,dJ,dL,dS,E,G,L,maxDA,maxDL,maxTA,n,xMX,xFAX,xqXX,yMX,yFAX,yqXX,Re,TX,u,xF,xS);


%%
o1_l = o1.a_d(2,:);
%--------------------
fig_1 = figure(1);
hold on
grid on
p_1 = plot(o1_l,o1.x_b(1,:),'r');
p_2 = plot(o1_l,o1.y_b(1,:),'--r');
p_3 = plot(o1_l,o1.b(1,:),'.r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
set(p_3,'LineWidth',lSize);
xlabel('l, m')
ylabel('M_g, N/m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Moment gn�cy wa� I')
legend('o� x','o� y','wypadkowy');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_1.png')
%------------------------------------------------------------------
fig_1 = figure(2);
hold on
grid on
p_1 = plot(o1_l,o1.d_b(1,:),'r');
p_2 = plot(o1_l,o1.d_st(2,:),'--r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
xlabel('l, m')
ylabel('d, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('�rednica wa�u I')
legend('minimalna','rzeczywista');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_2.png')
%-------------------------------------------------------------------
fig_1 = figure(3);
hold on
grid on
p_1 = plot(o1_l,o1.d_Wg(1,:),'r');
p_2 = plot(o1_l,o1.d_W0(1,:),'--r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
xlabel('l, m')
ylabel('-, -')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Wsp�czynniki geometryczne wa�u I')
legend('W_g','W_0');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_3.png')
%---------------------------------------------
fig_1 = figure(4);
hold on
grid on
p_1 = plot(o1_l,o1.d_I(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('I, m^4')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Geometryczny moment bezw�adno�ci wa�u I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_4.png')
%-------------------------------------------------------------
fig_1 = figure(5);
hold on
grid on
p_1 = plot(o1_l,o1.sigma(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\sigma, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enia normalne w wale I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_5.png')
%---------------------------------------------------------------
fig_1 = figure(6);
hold on
grid on
p_1 = plot(o1_l,o1.a_d(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\psi, rad')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('K�t ugi�cia wa�u I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_6.png')
%---------------------------------------------------------------
fig_1 = figure(7);
hold on
grid on
p_1 = plot(o1_l,o1.l_d(1,:)-min(o1.l_d(1,:)),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('y, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Linia ugi�cia wa�u I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_7.png')
%---------------------------------------------------------------
fig_1 = figure(8);
hold on
grid on
p_1 = plot(o1_l,o1.tau(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\tau, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enia styczne w wale I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_8.png')
%---------------------------------------------------------------
fig_1 = figure(9);
hold on
grid on
p_1 = plot(o1_l,o1.a_t(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\xi, rad')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('K�t skr�cenia wa�u I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_9.png')
%---------------------------------------------------------------
fig_1 = figure(10);
hold on
grid on
p_1 = plot(o1_l,o1.sigma_HM(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\sigma_{HM}, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enie wyt�eniowe wa�u I')
legend(sprintf('wsp. bezp. = %.0f',o1.sf));
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_10.png')
%-----------------------------------
fig_1 = figure(31);
hold on
grid on
p_1 = plot(o1_l,o1.t(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('T, N/m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Moment skrecj�cy wa� I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_21.png')
%-----------------------------------
fig_1 = figure(34);
hold on
grid on
p_1 = plot(o1_l,o1.w_kr(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\omega, rad/s')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Pr�dko�� krytyczna wa�u I')
legend(sprintf('w_{min} = %.0f',min(o1.w_kr(1,:))));
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_I/fig_22.png')
%%
o2_l = o2.a_d(2,:);
%--------------------
fig_1 = figure(11);
hold on
grid on
p_1 = plot(o2_l,o2.x_b(1,:),'r');
p_2 = plot(o2_l,o2.y_b(1,:),'--r');
p_3 = plot(o2_l,o2.b(1,:),':r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
set(p_3,'LineWidth',lSize);
xlabel('l, m')
ylabel('M_g, N/m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Moment gn�cy wa� II')
legend('o� x','o� y','wypadkowy');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_21.png')
%------------------------------------------------------------------
fig_1 = figure(12);
hold on
grid on
p_1 = plot(o2_l,o2.d_b(1,:),'r');
p_2 = plot(o2_l,o2.d_st(2,:),'--r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
xlabel('l, m')
ylabel('d, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('�rednica wa�u II')
legend('minimalna','rzeczywista');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_12.png')
%-------------------------------------------------------------------
fig_1 = figure(13);
hold on
grid on
p_1 = plot(o2_l,o2.d_Wg(1,:),'r');
p_2 = plot(o2_l,o2.d_W0(1,:),'--r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
xlabel('l, m')
ylabel('-, -')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Wsp�czynniki geometryczne wa�u II')
legend('W_g','W_0');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_13.png')
%---------------------------------------------
fig_1 = figure(14);
hold on
grid on
p_1 = plot(o2_l,o2.d_I(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('I, m^4')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Geometryczny moment bezw�adno�ci wa�u II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_14.png')
%-------------------------------------------------------------
fig_1 = figure(15);
hold on
grid on
p_1 = plot(o2_l,o2.sigma(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\sigma, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enia normalne w wale II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_15.png')
%---------------------------------------------------------------
fig_1 = figure(16);
hold on
grid on
p_1 = plot(o2_l,o2.a_d(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\psi, rad')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('K�t ugi�cia wa�u II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_16.png')
%---------------------------------------------------------------
fig_1 = figure(17);
hold on
grid on
p_1 = plot(o2_l,o2.l_d(1,:)-min(o2.l_d(1,:)),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('y, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Linia ugi�cia wa�u II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_17.png')
%---------------------------------------------------------------
fig_1 = figure(18);
hold on
grid on
p_1 = plot(o2_l,o2.tau(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\tau, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enia styczne w wale II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_18.png')
%---------------------------------------------------------------
fig_1 = figure(19);
hold on
grid on
p_1 = plot(o2_l,o2.a_t(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\xi, rad')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('K�t skr�cenia wa�u II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_19.png')
%---------------------------------------------------------------
fig_1 = figure(20);
hold on
grid on
p_1 = plot(o2_l,o2.sigma_HM(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\sigma_{HM}, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enie wyt�eniowe wa�u II')
legend(sprintf('wsp. bezp. = %.0f',o2.sf));
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_20.png')
%-----------------------------------
fig_1 = figure(32);
hold on
grid on
p_1 = plot(o2_l,o2.t(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('T, N/m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Moment skrecj�cy wa� II')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_21.png')
%-----------------------------------
fig_1 = figure(35);
hold on
grid on
p_1 = plot(o2_l,o2.w_kr(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\omega, rad/s')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Pr�dko�� krytyczna wa�u II')
legend(sprintf('w_{min} = %.0f',min(o2.w_kr(1,:))));
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_II/fig_22.png')
%%
o3_l = o3.a_d(2,:);
%--------------------
fig_1 = figure(21);
hold on
grid on
p_1 = plot(o3_l,o3.x_b(1,:),'r');
p_2 = plot(o3_l,o3.y_b(1,:),'--r');
p_3 = plot(o3_l,o3.b(1,:),'.r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
set(p_3,'LineWidth',lSize);
xlabel('l, m')
ylabel('M_g, N/m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Moment gn�cy wa� III')
legend('o� x','o� y','wypadkowy');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_1.png')
%------------------------------------------------------------------
fig_1 = figure(22);
hold on
grid on
p_1 = plot(o3_l,o3.d_b(1,:),'r');
p_2 = plot(o3_l,o3.d_st(2,:),'--r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
xlabel('l, m')
ylabel('d, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('�rednica wa�u III')
legend('minimalna','rzeczywista');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_2.png')
%-------------------------------------------------------------------
fig_1 = figure(23);
hold on
grid on
p_1 = plot(o3_l,o3.d_Wg(1,:),'r');
p_2 = plot(o3_l,o3.d_W0(1,:),'--r');
set(p_1,'LineWidth',lSize);
set(p_2,'LineWidth',lSize);
xlabel('l, m')
ylabel('-, -')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Wsp�czynniki geometryczne wa�u III')
legend('W_g','W_0');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_3.png')
%---------------------------------------------
fig_1 = figure(24);
hold on
grid on
p_1 = plot(o3_l,o3.d_I(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('I, m^4')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Geometryczny moment bezw�adno�ci wa�u III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_4.png')
%-------------------------------------------------------------
fig_1 = figure(25);
hold on
grid on
p_1 = plot(o3_l,o3.sigma(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\sigma, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enia normalne w wale III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_5.png')
%---------------------------------------------------------------
fig_1 = figure(26);
hold on
grid on
p_1 = plot(o3_l,o3.a_d(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\psi, rad')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('K�t ugi�cia wa�u III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_6.png')
%---------------------------------------------------------------
fig_1 = figure(27);
hold on
grid on
p_1 = plot(o3_l,o3.l_d(1,:)-min(o3.l_d(1,:)),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('y, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Linia ugi�cia wa�u III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_7.png')
%---------------------------------------------------------------
fig_1 = figure(28);
hold on
grid on
p_1 = plot(o3_l,o3.tau(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\tau, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enia styczne w wale III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_8.png')
%---------------------------------------------------------------
fig_1 = figure(29);
hold on
grid on
p_1 = plot(o3_l,o3.a_t(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\xi, rad')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('K�t skr�cenia wa�u III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_9.png')
%---------------------------------------------------------------
fig_1 = figure(30);
hold on
grid on
p_1 = plot(o3_l,o3.sigma_HM(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\sigma_{HM}, Pa')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Napr�enie wyt�eniowe wa�u III')
legend(sprintf('wsp. bezp. = %.0f',o3.sf));
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_10.png')
%-----------------------------------
fig_1 = figure(33);
hold on
grid on
p_1 = plot(o3_l,o3.t(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('T, N/m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Moment skrecj�cy wa� III')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_21.png')
%-----------------------------------
fig_1 = figure(36);
hold on
grid on
p_1 = plot(o3_l,o3.w_kr(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('\omega, rad/s')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Pr�dko�� krytyczna wa�u III')
legend(sprintf('w_{min} = %.0f',min(o3.w_kr(1,:))));
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/shaft_III/fig_22.png')