close all
clear

fSize = 30;      % wielkosc liter
lSize = 2;         % gr. linii

% 1. Geom. parameters
alpha = 20;
b = 0.05;
beta = 0;
c_ = .25;
m = HelicalGears.m_I(18);
m2 = HelicalGears.m_I(20);
x1 = 0;
x2 = 0;

% 6. Kinematic
eta = .916;
N = 22000*eta;
n = 1460; %obr/min
nr = 2*pi*n/60; % rad/s
Ka = 1.3;

i = 21;
i1 = 4.25;
i2 = i/i1;


G = Gearmotor;
G2 = Gearmotor;

G = G.geometry(alpha,b,beta,c_,m,x1,x2);
G2 = G2.geometry(alpha,b,10,c_,m2,x1,x2);

G.G_1 = G.G_1.teethNoumber(20);
%G.G_1 = G.G_1.teethDiameter(.02);
G.G_1 = G.G_1.speedRotation(nr);

% 2. Gear
G = G.gearGeometrical(i1);

% 3. Axieses distance
G = G.axiesesDistance;
G = G.coefficientMaterial;

% 4. Diameters
G = G.diameterRolling;

% 5. Interlocking
G = G.coefficientInterlocking;

% 6. Kin
G =G.forceGear(N,Ka);


G2.G_1 = G2.G_1.teethDiameter(0.08);
G2.G_1 = G2.G_1.speedRotation(G.G_2.w);
% 2. Gear
G2 = G2.gearGeometrical(i2);
% 3. Axieses distance
G2 = G2.axiesesDistance;
G2 = G2.coefficientMaterial;
% 4. Diameters
G2 = G2.diameterRolling;
% 5. Interlocking
G2 = G2.coefficientInterlocking;
% 6. Kin
G2 =G2.forceGear(N,Ka);

%%
fig_1 = figure(1);
hold on
grid on
p_1 = plot(G.G_1.s(2,:),G.G_1.s(1,:),'r');
set(p_1,'LineWidth',lSize);
xlabel('l, m')
ylabel('s, m')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Szeroko�� z�ba z�bnika I')
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'figs/gear_I/fig_10png')
